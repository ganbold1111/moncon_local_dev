# -*- coding: utf-8 -*-
# Part of Asterisk technologies. See LICENSE file for full copyright and licensing details.
{

    'name': 'Moncon - Cash expense',
    'version': '1.0',
    'author': 'Asterisk Technologies LLC',
    "description": """
     Уг модуль нь Монконы бэлэн мөнгөний хүсэлтийн модулыг удамшуулсан болно.
 """,
    'website': 'http://www.asterisk-tech.mn',
    'images': [''],
    'depends': ['l10n_mn_cash_expense'],
    'data': [
        'views/moncon_cash_expense_view.xml'
    ],
    'installable': True,
    'auto_install': False
}


