# -*- coding: utf-8 -*-
{
    'name': "MONCON Contract Management Module",
    'version': '1.0',
    'depends': ['account','l10n_mn_workflow_config','project','l10n_mn_report', 'l10n_mn_contract'],
    'author': "MONCON",
    'category': 'Mongolian Modules',
    'description': """
    Description texts
    """,
    'data': [
        'security/security.xml',
        'views/project_view.xml',
    ]
}