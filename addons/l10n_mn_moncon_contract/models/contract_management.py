# -*- coding: utf-8 -*-
import logging

import odoo.addons.decimal_precision as dp
from odoo import SUPERUSER_ID, _, api, exceptions, fields, models

_logger = logging.getLogger(__name__)


class ContractManagement(models.Model):
    _inherit = 'contract.management'

    amount_without_tax = fields.Float('Amount without tax', required=True, digits=dp.get_precision('Contract'))
    after_handover_rate = fields.Float('Rate collateral after hand over')
    after_handover_amount = fields.Float('Amount collateral after hand over')
    ledge_rate = fields.Float('Pledge rate', digits=dp.get_precision('Contract'))
    pledge_amount = fields.Float('Pledge amount')
    analytic_account_id = fields.Many2one('account.analytic.account', string='Expense category',
                                          help="Link this project to an analytic account if you need financial management on contracts. ",
                                          track_visibility='onchange', domain="[('type','=','normal')]")
    project_id = fields.Many2one('project.project', 'Project name', domain="[('user_id','!=', False)]")
    project_code = fields.Char(related="project_id.concise_name", readonly=True, default=False, store=True)
    pledge_rate = fields.Float('Pledge rate', digits=dp.get_precision('Contract'))
    procurement_employee = fields.Many2one('hr.employee', 'Procurement Employee')
    is_additional = fields.Boolean('Is additional')
    main_contract_id = fields.Many2one('contract.management', 'Main contract', domain=[('is_additional', '=', False)])
    old_name = fields.Char('Old Name')
