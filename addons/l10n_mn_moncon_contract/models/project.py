# -*- coding: utf-8 -*-


from odoo import api, fields, models, _


class Project(models.Model):
    _inherit = 'project.project'

    financial_analyst_ids = fields.Many2many('res.users', 'project_financial_user_rel', 'project_id', 'user_id', 'Financial Analysts')
    booker_ids = fields.Many2many('res.users', 'booker_user_rel', 'project_id', 'user_id', 'Booker')
    concise_name = fields.Char('Project concise name', required=True)
    journal_id = fields.Many2one('account.journal', 'Journal', domain=[('type', '=', 'bank')])
    main_contract_concise = fields.Char('Summary of the basic agreement with the client')
    project_address = fields.Char('Project address')


class Department(models.Model):
    _inherit = 'hr.department'

    financial_analyst_ids = fields.Many2many('res.users', 'dep_financial_user_rel', 'department_id', 'user_id', string='Financial Analysts')
    booker_ids = fields.Many2many('res.users', 'dep_user_rel', 'department_id', 'user_id', 'Booker')
    contract_user_ids = fields.Many2many('res.users', 'dep_contract_user_rel', 'department_id', 'user_id', string='Contract Users')
    journal_id = fields.Many2one('account.journal', 'Journal', domain=[('type', '=', 'bank')])