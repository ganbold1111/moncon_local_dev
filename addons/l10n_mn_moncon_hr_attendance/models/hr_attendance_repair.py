# -*- coding: utf-8 -*-
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
import pytz
from odoo.http import request
from datetime import date, datetime, timedelta
from calendar import monthrange
from odoo.addons.l10n_mn_web.models.time_helper import *
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class HrAttendanceRepair(models.Model):
    _inherit = 'hr.attendance.repair'

    hr_employee_id = fields.Many2one('hr.employee', related='employee_id.hr_employee_id', store=True, readonly=True, string='ХН ажилтан')
    cancel_date = fields.Date()

    @api.model
    def search(self, args, offset=0, limit=0, order=None, count=False):
        args = list(args)
        user = self.env['res.users'].browse(self._uid)
        for index in range(len(args)):
            if (type(args[index]) == list):
                if args[index][2]:
                    if args[index][2] == 'ODERP_DEPARTMENT':
                        args[index] = (args[index][0], args[index][1], user.allowed_department_ids.ids)
        return super(HrAttendanceRepair, self).search(args, offset=offset, limit=limit, order=order, count=count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        user = self.env['res.users'].browse(self._uid)
        domain = list(domain)
        for index in range(len(domain)):
            if (type(domain[index]) == list):
                if domain[index][2]:
                    if domain[index][2] == 'ODERP_DEPARTMENT':
                        domain[index] = (domain[index][0], domain[index][1], user.allowed_department_ids.ids)
        return super(HrAttendanceRepair, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.multi
    def cancel(self):
        tz = get_user_timezone(self.env.user)
        str_today = str(datetime.today().strftime('%Y-%m-%d'))
        today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d')).astimezone(tz)
        self.cancel_date = today + timedelta(days=7)
        return super(HrAttendanceRepair, self).cancel()


class HrAttendanceRepairLine(models.Model):
    _inherit = 'hr.attendance.repair.line'



    @api.model
    def create(self, vals):
        creation = super(HrAttendanceRepairLine, self).create(vals)

        if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days') and vals['date_of_attendance']:
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(vals['date_of_attendance'], '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)

            if today.weekday() == 0:
                str_start = today - timedelta(days=7)
            else:
                str_start = today - timedelta(today.weekday())
            if str_start.date() > date_from.date():
                raise UserError(u'Тухайн долоо хоногийн ирц нөхөн засах хүсэлтийн хугацаа хэтэрсэн байна!!!')
        return creation


    @api.multi
    def write(self, vals):
        if not self.env.user.has_group('l10n_mn_moncon_hr_attendance.group_attendance_holidays_not_days') and 'date_of_attendance' in vals:
            tz = get_user_timezone(self.env.user)
            date_from = pytz.utc.localize(datetime.strptime(vals['date_of_attendance'], '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            str_today = str(datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
            today = pytz.utc.localize(datetime.strptime(str_today, '%Y-%m-%d %H:%M:%S')).astimezone(tz)
            if today.weekday() == 0:
                str_start = today - timedelta(days=7)
            else:
                str_start = today - timedelta(today.weekday())
            if self.attendance_repair.cancel_date:
                if datetime.strptime(self.attendance_repair.cancel_date, '%Y-%m-%d').date() < date_from.date():
                    raise UserError(u'7 хоног хэтэрсэн байна.')
            else:
                if str_start.date() > date_from.date():
                    raise UserError(u'Тухайн долоо хоногийн ирц нөхөн засах хүсэлтийн хугацаа хэтэрсэн байна!!!')
        return super(HrAttendanceRepairLine, self).write(vals)
