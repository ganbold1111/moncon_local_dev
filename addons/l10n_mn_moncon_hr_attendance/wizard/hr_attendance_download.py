# -*- coding: utf-8 -*-

import logging
from datetime import datetime, timedelta

import pytz
import time
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError
from odoo.addons.l10n_mn_web.models.time_helper import *

_logger = logging.getLogger(__file__)


class HrAttendanceDownload(models.TransientModel):
    _inherit = "hr.attendance.download"

    # @api.multi
    # def create_employee_attendances(self, employee, attendances, date_from_att=False, date_to_att=False):
    #     user_time_zone = pytz.timezone(self.env.user.tz)
    #     utc = pytz.UTC

    #     date = None
    #     day_attendance = self.env['hr.attendance']
    #     for att in attendances:
    #         if int(att.timestamp.strftime('%H')) < 6:
    #             from_dt = datetime.combine(att.timestamp.date(), datetime.min.time())
    #             to_dt = datetime.combine(att.timestamp.date() + timedelta(days=1), datetime.min.time())
    #             from_dt_str = user_time_zone.localize(from_dt).astimezone(utc).strftime('%Y-%m-%d 00:00:00')
    #             to_dt_str = (user_time_zone.localize(to_dt).astimezone(utc)).strftime('%Y-%m-%d 3:00:00')
    #             attendance_ids = self.env['hr.attendance'].search(
    #                 [('employee_id', '=', employee.id), ('check_in', '>=', from_dt_str), ('check_in', '<', to_dt_str)],
    #                 order='check_in')
    #             _logger.info(u'ирцийн ids %s%s%s', attendance_ids, from_dt_str,to_dt_str)
    #             att_in_utc_time = user_time_zone.localize(att.timestamp).astimezone(utc).replace(tzinfo=None)
    #             att_in_utc = user_time_zone.localize(att.timestamp).astimezone(utc).replace(tzinfo=None)
    #             if attendance_ids:
    #                 for attendance_id in attendance_ids:
    #                     att_in_utc_time = user_time_zone.localize(att.timestamp).astimezone(utc).replace(tzinfo=None)
    #                     attendance_id.check_out = att_in_utc_time
    #                     attendance_id.out_time = att.timestamp.strftime('%H:%M')
    #                     attendance_id.out_device_name = self.get_device_name(get_day_like_display(att_in_utc, self.env.user), attendances)
    #             else:
    #                 self.env['hr.attendance'].create({
    #                     'employee_id': employee.id,
    #                     'check_in': att_in_utc_time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
    #                     'check_out': (att_in_utc_time + timedelta(seconds=1)).strftime(DEFAULT_SERVER_DATETIME_FORMAT),
    #                     'in_device_name': self.get_device_name(get_day_like_display(att_in_utc, self.env.user),
    #                                                            attendances),
    #                     'company_id': employee.company_id.id,
    #                     'in_time': att.timestamp.strftime('%H:%M'),
    #                 })
    #         else:
    #             if date != att.timestamp.date():
    #                 date = att.timestamp.date()

    #                 # check day attendance
    #                 existing_attendances = self.get_attendances_of_day(employee, date)
    #                 if existing_attendances:
    #                     day_attendance = existing_attendances[0]
    #                     if len(existing_attendances) > 1:
    #                         (existing_attendances - day_attendance).unlink()

    #                 att_in_utc = user_time_zone.localize(att.timestamp).astimezone(utc).replace(tzinfo=None)
    #                 if existing_attendances and day_attendance:
    #                     # device attendance is earlier than ERP one
    #                     if day_attendance.check_in:
    #                         if att_in_utc < datetime.strptime(day_attendance.check_in, DEFAULT_SERVER_DATETIME_FORMAT):
    #                             day_attendance.check_in = att_in_utc
    #                     else:
    #                         day_attendance.check_in = att_in_utc
    #                 else:
    #                     day_attendance = self.env['hr.attendance'].create({
    #                         'employee_id': employee.id,
    #                         'check_in': att_in_utc.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
    #                         'check_out': (att_in_utc + timedelta(seconds=1)).strftime(DEFAULT_SERVER_DATETIME_FORMAT),
    #                         'in_device_name': self.get_device_name(get_day_like_display(att_in_utc, self.env.user), attendances),
    #                         'company_id': employee.company_id.id,
    #                         'in_time': att.timestamp.strftime('%H:%M'),
    #                     })
    #             else:
    #                 att_in_utc = user_time_zone.localize(att.timestamp).astimezone(utc).replace(tzinfo=None)
    #                 # device attendance is later than ERP one
    #                 if day_attendance.check_out:
    #                     if att_in_utc > datetime.strptime(day_attendance.check_out, DEFAULT_SERVER_DATETIME_FORMAT):
    #                         day_attendance.check_out = att_in_utc
    #                         day_attendance.out_time = att.timestamp.strftime('%H:%M')
    #                         day_attendance.out_device_name = self.get_device_name(get_day_like_display(att_in_utc, self.env.user), attendances)
