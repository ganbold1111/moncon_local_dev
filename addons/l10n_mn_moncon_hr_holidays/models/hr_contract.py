# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class HrContract(models.Model):
    _inherit = 'hr.contract'

    moncon_nohon_holiday_limit_day = fields.Float('Нөхөн амралтын хязгаар', default=56)
    is_moncon_overtime_limit = fields.Boolean('Илүү цаг тооцох эсэх', default=False)
    total_moncon_holiday_limit = fields.Float(string='Нөхөн амрах илүү цагийн хязгаар')
