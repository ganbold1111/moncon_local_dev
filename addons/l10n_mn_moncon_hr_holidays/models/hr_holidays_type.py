# -*- coding: utf-8 -*-

from odoo import _, api, fields, models

class HolidaysType(models.Model):

    _inherit = "hr.holidays.status"

    is_nohon_amrakh = fields.Boolean(u'Нөхөн амрах')

    class HolidaysType(models.Model):

        _inherit = "hr.holidays.status"

        @api.multi
        def get_days(self, employee_id):
            # need to use `dict` constructor to create a dict per id
            result = dict((id, dict(max_leaves=0, leaves_taken=0, remaining_leaves=0, virtual_remaining_leaves=0)) for id in self.ids)

            holidays = self.env['hr.holidays'].search([
                ('employee_id', '=', employee_id),
                ('state', 'in', ['confirm', 'validate1', 'validate']),
                ('holiday_status_id', 'in', self.ids)
            ])

            for holiday in holidays:
                status_dict = result[holiday.holiday_status_id.id]
                if holiday.type == 'add':
                    if holiday.state == 'validate':
                        # note: add only validated allocation even for the virtual
                        # count; otherwise pending then refused allocation allow
                        # the employee to create more leaves than possible
                        status_dict['virtual_remaining_leaves'] += holiday.number_of_hours_temp
                        status_dict['max_leaves'] += holiday.number_of_hours_temp
                        status_dict['remaining_leaves'] += holiday.number_of_hours_temp
                elif holiday.type == 'remove':  # number of days is negative
                    if holiday.state == 'validate':
                        status_dict['leaves_taken'] += holiday.number_of_hours_temp
                        status_dict['remaining_leaves'] -= holiday.number_of_hours_temp
            return result