# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.addons.l10n_mn_web.models.time_helper import *
from xlsxwriter.utility import xl_rowcol_to_cell
import xlsxwriter
from io import BytesIO
import base64
import time
from datetime import datetime, timedelta
from odoo.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles


class HourBalancePrint(models.TransientModel):
    _inherit = "hour.balance.print"

    @api.model
    def default_get(self, fields):
        res = super(HourBalancePrint, self).default_get(fields)
        if self._context.get('active_ids'):
            b_obj = self.env['hour.balance']
            balance = b_obj.browse(self._context['active_ids'])
            res.update({
                'balance_ids': [(6, 0, balance.ids)]
            })
        return res

    balance_ids = fields.Many2many('hour.balance', string="Цагийн баланс")
            
    @api.multi
    def export(self):
        for obj in self:
            context = dict(self._context or {})
            sheetname_1 = 'Balance'
            
            now = time.strftime('%Y-%m-%d')
            output = BytesIO()
            
            workbook = xlsxwriter.Workbook(output)
            worksheet = workbook.add_worksheet(sheetname_1)
            is_training = False
            module = self.sudo().env['ir.module.module'].search([('name', '=', 'l10n_mn_hr_hour_balance_training')])
            if module:
                if module.state == 'installed':
                    is_training = True

            footer = workbook.add_format(ReportExcelCellStyles.format_footer_float)
            
            title = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'left',
                'valign': 'vcenter',
                'font_size': 9,
                'font_name': 'Calibri',
                })
            sub_title = workbook.add_format({
                'bold': 1,
                'border': 0,
                'align': 'center',
                'valign': 'vcenter',
                'font_size': 14,
                'font_name': 'Calibri',
                })
            sub_title1 = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'font_size': 9,
                'bg_color': '#99ccff',
                'font_name': 'Calibri',
                'text_wrap':1,
                })
            sub_title_left = workbook.add_format({
                'bold': 1,
                'border': 1,
                'align': 'left',
                'valign': 'vcenter',
                'font_size': 9,
                'bg_color': '#b9cde5',
                'font_name': 'Calibri',
                'text_wrap':1,
                })
            header = workbook.add_format({
                'border': 1,
                'bold': 1,
                'align': 'center',
                'valign': 'vcenter',
                'text_wrap': 'on',
                'font_size':8,
                'font_name': 'Times New Roman',
                })
            cell_format_left = workbook.add_format({
                'border': 1,
                'align': 'left',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                'text_wrap':1,
                })
            cell_format_right = workbook.add_format({
                'border': 1,
                'align': 'right',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                'text_wrap':1,
                'num_format': '0.00'
                })
            cell_format_center = workbook.add_format({
                'border': 1,
                'align': 'center',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                'text_wrap':1,
                })
            cell_format_footer = workbook.add_format({
                'align': 'left',
                'valign': 'vcenter',
                'font_size':9,
                'font_name': 'Times New Roman',
                })

            row = 0
            col = 0
            colh = 0
            worksheet.freeze_panes(row + 5, col + 1)
            worksheet.set_zoom(90)
            worksheet.set_column('A:A', 3)
            worksheet.set_column('B:B', 12)
            worksheet.set_column('C:C', 12)
            worksheet.set_column('D:D', 10)
            worksheet.set_column('E:E', 17)
            worksheet.set_column('F:F', 6)
            worksheet.set_column('G:G', 6)
            worksheet.set_column('H:H', 6)
            worksheet.set_column('I:I', 6)
            if is_training == True or (obj.balance_ids[0].company_id.confirm_overtime) if obj.balance_ids else False:
                worksheet.set_column('H:AD', 6)
                worksheet.set_column('AE:AE', 15)
            elif is_training == True and (obj.balance_ids[0].company_id.confirm_overtime) if obj.balance_ids else False:
                worksheet.set_column('H:AE', 6)
                worksheet.set_column('AF:AF', 15)
            else:
                worksheet.set_column('H:AC', 6)
                worksheet.set_column('AD:AD', 15)
                
            if colh == 1:
                worksheet.set_column('H:AD', 6)
                worksheet.set_column('AE:AE', 15)
            if colh == 2:
                if is_training == True:
                    worksheet.set_column('H:AF', 6)
                    worksheet.set_column('AG:AG', 15)
                else:
                    worksheet.set_column('H:AE', 6)
                    worksheet.set_column('AF:AF', 15)
            worksheet.set_row(1, 41)
            worksheet.set_row(2, 25)
            worksheet.set_row(3, 25)
            worksheet.set_row(4, 20)
            worksheet.set_row(5, 20)
            
            # title
            worksheet.write(row, col+2, u'%s' % (obj.balance_ids[0].company_id.name) if obj.balance_ids else '', title)
            worksheet.write(row, colh  , u'Огноо: %s' % (now), title)
            row += 1
            balance_names = ',\n'.join('(%s)' % r.name for r in obj.balance_ids)
            worksheet.merge_range(row, col, row, col+colh+27, u'(%s)-Цагийн баланс' % (balance_names), sub_title)
            row += 1
            worksheet.merge_range(row, col, row+2, col, u'Д/д', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Нэр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Овог', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Регистр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Албан тушаал', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Ажиллавал зохих өдөр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Ажиллавал зохих цаг', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Ажилласан өдөр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Ажилласан цаг', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Нөхөж амарсан цаг', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row + 2, col, u'Хоцролт /цаг/', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Хоцролт /минут/', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Таслалт/цаг/', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Цалингүй чөлөө /цаг/', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row+2, col, u'Цалинтай чөлөө /цаг/', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row + 2, col, u'Өвчтөй/цаг/', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Томилолт /цаг/', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row+2, col, u'Нийт баталгаажуулах/цаг/', sub_title1)
            col +=1 
            worksheet.merge_range(row, col, row+2, col, u'Талбайн хоног', sub_title1)
            col +=1 
            worksheet.merge_range(row, col, row+2, col, u'Хээрийн хоног', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row+2, col, u'Нөхөж амрах хуримтлагдсан цаг', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row + 2, col, u'Нөхөж амрах илүү цагаар тооцуулах цаг', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Илүү цаг (Шувуу)', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Ээлжийн амралт /цаг/', sub_title1)
            col +=1
            worksheet.merge_range(row, col, row + 2, col, u'Нийт KPI', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Үндсэн батлагдсан цаг', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Цагийн батлагдсан цаг', sub_title1)
            col += 1
            # worksheet.merge_range(row, col, row + 2, col, u'Шувуу урьдчилгааны хувь', sub_title1)
            # col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Батлагдсан өдөр', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row+2, col, u'Цалин олголт хийхгүй эсэх', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'Тайлбар', sub_title1)
            col += 1
            worksheet.merge_range(row, col, row + 2, col, u'ХН ажилтан', sub_title1)
            row += 3
            department_ids = []
            for balance in obj.balance_ids:
                for line in balance.balance_line_ids:
                    if line.department_id.id not in department_ids:
                        department_ids.append(line.department_id.id)
                
            number = 1
            department_ids.sort()
            department_ids_add_number = len(department_ids)
            # column
            
            if is_training == True:
                colh +=1               
            if (obj.balance_ids[0].company_id.confirm_overtime) if obj.balance_ids else False:
                colh +=1

            for department_id in department_ids:
                col = 0
                department = self.env['hr.department'].browse(department_id)
                worksheet.merge_range(row, col, row, col+colh+29, department.name, sub_title_left)
                row += 1
                for b in obj.balance_ids:
                    for line in b.balance_line_ids:
                        if department_id == line.department_id.id:
                            col = 0
                            worksheet.write(row, col, number, cell_format_center)
                            col +=1
                            empname = ""
                            lastname = ""
                            if line.employee_id.name:
                                empname += line.employee_id.name
                            if line.employee_id.last_name:
                                lastname += line.employee_id.last_name
                            register = ""
                            if line.employee_id.ssnid:
                                register = line.employee_id.ssnid
                            job = ""
                            if line.employee_id.job_id.name:
                                job = line.employee_id.job_id.name
                            worksheet.write(row, col, empname, cell_format_left)
                            col +=1
                            worksheet.write(row, col, lastname, cell_format_left)
                            col += 1
                            worksheet.write(row, col, register, cell_format_left)
                            col +=1
                            worksheet.write(row, col, job, cell_format_left)
                            col +=1
                            worksheet.write(row, col, line.reg_day, cell_format_right)
                            col +=1
                            worksheet.write(row, col, line.reg_hour, cell_format_right)
                            col +=1
                            worksheet.write(row, col, line.worked_day, cell_format_right)
                            col +=1
                            worksheet.write(row, col, line.worked_hour, cell_format_right)
                            col +=1
                            worksheet.write(row, col, line.rehabilitated_holiday, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.lag_minute, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.lag_hour, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.missed_hour_moncon, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.unpaid_holiday, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.paid_holiday, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.sick_leave, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.business_trip_hour, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.total_worked_hour, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.field_count_day, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.country_count_day, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.rehabilitated_add_holiday, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.rehabilitated_money_holiday, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.moncon_overtime, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.annual_leave, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.kpi_total_point, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.advance_percent, cell_format_right)
                            col += 1
                            worksheet.write(row, col, line.advance_day, cell_format_right)
                            col += 1
                            # worksheet.write(row, col, line.advance_hour, cell_format_right)
                            # col += 1
                            worksheet.write(row, col, line.advance_wage_hour, cell_format_right)
                            col += 1
                            if line.is_close_salary:
                                worksheet.write(row, col, u'Тийм', cell_format_left)
                                col += 1
                            else:
                                worksheet.write(row, col, u'', cell_format_left)
                                col += 1
                            if line.description:
                                worksheet.write(row, col, line.description, cell_format_left)
                                col += 1
                            else:
                                worksheet.write(row, col, u'', cell_format_left)
                                col += 1
                            if line.balance_id.hr_employee_id:
                                worksheet.write(row, col, line.balance_id.hr_employee_id.name, cell_format_right)
                            else:
                                worksheet.write(row, col, u'', cell_format_left)
                            row += 1
                            number += 1


                            worksheet.write(row, 0, '', footer)
                            worksheet.write(row, 1, '', footer)
                            worksheet.write(row, 2, '', footer)
                            worksheet.write(row, 3, '', footer)
                            worksheet.write(row, 4, _(u'Нийт: '), footer)
                            worksheet.write_formula(row, 5, '{=SUM(F7:F' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 6, '{=SUM(G7:G' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 7, '{=SUM(H7:H' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 8, '{=SUM(I7:I' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 9, '{=SUM(J7:J' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 10, '{=SUM(K7:K' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 11, '{=SUM(L7:L' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 12, '{=SUM(M7:M' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 13, '{=SUM(N7:N' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 14, '{=SUM(O7:O' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 15, '{=SUM(P7:P' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 16, '{=SUM(Q7:Q' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 17, '{=SUM(R7:R' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 18, '{=SUM(S7:S' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 19, '{=SUM(T7:T' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 20, '{=SUM(U7:U' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 21, '{=SUM(V7:V' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 22, '{=SUM(W7:W' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 23, '{=SUM(X7:X' + str(number + 6 + department_ids_add_number - 2) + ')}', footer)
                            worksheet.write_formula(row, 24, '{=SUM(Y7:Y' + str(number + 6 + department_ids_add_number - 2) + ')}',footer)
                            worksheet.write_formula(row, 25, '{=SUM(Z7:Z' + str(number + 6 + department_ids_add_number - 2) + ')}',footer)
                            worksheet.write_formula(row, 26, '{=SUM(AA7:AA' + str(number + 6 + department_ids_add_number - 2) + ')}',footer)
                            worksheet.write_formula(row, 27, '{=SUM(BB7:BB' + str(number + 6 + department_ids_add_number - 2) + ')}',footer)
                            worksheet.write(row, 28, '', footer)
                            worksheet.write(row, 29, '', footer)
                            worksheet.write(row, 30, '', footer)


                
            row += 1
            worksheet.write(row+1, 1  , u'Бэлтгэсэн хүний нөөцийн менежер: ........................................', cell_format_footer)
            row += 1    
            worksheet.write(row+1, 1  , u'Хүлээж авсан цалингийн нягтлан бодогч: ....................................', cell_format_footer)
            
            workbook.close()
            out = base64.encodestring(output.getvalue())
            file_name = u'Цагийн баланс_%s' % now

            excel_id = self.env['oderp.report.excel.output'].create({'filedata': out, 'filename': file_name+'.xlsx'})
            
            return {
                'name': 'Export Result',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'oderp.report.excel.output',
                'res_id': excel_id.id,
                'view_id': False,
                'context': self._context,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'nodestroy': True,
            }
