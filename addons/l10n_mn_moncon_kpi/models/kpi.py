# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from openerp.addons.l10n_mn_report.tools.report_excel_cell_styles import ReportExcelCellStyles
from odoo.addons.l10n_mn_report.models.report_helper import *
import base64
from io import BytesIO
import time
import xlsxwriter


class Kpi(models.Model):

    _name = 'moncon.kpi'
    _description = 'KPI'
    _inherit = ['mail.thread', 'ir.needaction_mixin']


    @api.depends('period_id')
    def compute_balance_name(self):
        for record in self:
            name = ''
            name_change = False
            if record.period_id:
                name += _("%s сарын KPI үнэлгээ") % record.period_id.name
                name_change = True
            if name_change:
                record.name = name

    name = fields.Char(string='Name', size=256, track_visibility='onchange', compute='compute_balance_name')
    period_id = fields.Many2one('account.period', string="KPI Month", domain=[('state', 'not in', ['done'])], track_visibility='onchange')
    kpi_state = fields.Selection([('draft', 'New'), ('done', 'Approved'), ], string='Төлөв', default='draft')
    kpi_lines = fields.One2many('moncon.kpi.line', 'kpi_line_id', string='Kpi employee lines')
    employee_ids = fields.Many2many('hr.employee', 'moncon_kpi_employee_rel', 'kpi_id', 'emp_id', string='ХН ажилтан')
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)


    participant_employee_ids = fields.Many2many('hr.employee', string="Participating Employees",
                                                domain="[('state_id.type','not in',('maternity','resigned', 'retired'))]")

    def action_send_mail_kpi_valuation(self):
        for kpi in self:
            group_kpi_manager = self.env.ref('l10n_mn_moncon_kpi.group_kpi_manager').id
            user_ids = self.env['res.users'].search([('groups_id', 'in', [group_kpi_manager])])
            model_obj = self.env['ir.model.data']
            ctx = {
                'name': kpi.name,
                'month': kpi.period_id.name,
                'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
                'action_id': model_obj.get_object_reference('l10n_mn_moncon_kpi', 'action_moncon_kpi_tree_view')[1],
                'id': kpi.id,
                'db_name': request.session.db,
                'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
            }

            template_id = self.env.ref('l10n_mn_moncon_kpi.action_send_kpi_email_template')

            if user_ids:
                user_emails = []
                for user in user_ids:
                    user_emails.append(user.login)
                    template_id.with_context(ctx).send_mail(user.id, force_send=True)
                email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
                        '<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
                kpi.message_post(body=email)


    #KPI мөрийн ажилтны төлөв батлагдсан биш байвал мэссэж өгөх.
    def finish_kpi_done(self):
        for rec in self:
            if rec.kpi_lines:
                for line in rec.kpi_lines:
                    if line.kpi_line_state1 != 'done' or line.kpi_line_state2 != 'done':
                        raise ValidationError(_('Error ! Worker state is not done!!!.'))
                    else:
                        False
            else:
                 False
            rec.kpi_state = 'done'

    def cancel_kpi_draft(self):
        for rec in self:
            rec.kpi_state = 'draft'


    @api.multi
    def add_employees(self):
        for obj in self:
            kpi_line_employee_ids = []
            for line_employee in obj.kpi_lines:
                kpi_line_employee_ids.append(line_employee.employee_id.id)
            for hr_employee in obj.employee_ids:
                employees = self.env['hr.employee'].search([('kpi_check', '=', 'True'),
                                                            ('hr_employee_id', '=', hr_employee.id),
                                                            ('state_id.type', '!=', 'resigned')])
                for emp in employees:
                    if emp.id not in kpi_line_employee_ids:
                        self.env['moncon.kpi.line'].create({'employee_id': emp.id,
                                                            'kpi_line_id': obj.id,
                                                            'kpi_percent_weight1': emp.kpi_percent_weight1,
                                                            'approve_user_id1': emp.approve_user_id1.id,
                                                            'kpi_percent_weight2': emp.kpi_percent_weight2,
                                                            'approve_user_id2': emp.approve_user_id2.id,
                                                            'period_id': self.period_id.id,
                                                            'kpi_line_state1': 'done' if not emp.approve_user_id1.id else 'draft',
                                                            'kpi_line_state2': 'done' if not emp.approve_user_id2.id else 'draft'})

    # def add_participates_view(self):
    #     for obj in self:
    #         view = obj.env.ref('l10n_mn_moncon_kpi.add_employee_view')
    #         return {
    #             'name': _('Add participants'),
    #             'type': 'ir.actions.act_window',
    #             'view_type': 'form',
    #             'view_mode': 'form',
    #             'res_model': 'moncon.kpi',
    #             'views': [(view.id, 'form')],
    #             'view_id': view.id,
    #             'target': 'new',
    #             'res_id': self.id
    #         }
    #KPI эксел тайлан
    def report_excel(self):
        # create workbook
        output = BytesIO()
        book = xlsxwriter.Workbook(output)

        # create name
        report_name = _('Kpi report')
        file_name = "%s_%s.xls" % (report_name, time.strftime('%Y%m%d_%H%M'),)

        # create formats
        format_name = book.add_format(ReportExcelCellStyles.format_name)
        format_filter = book.add_format(ReportExcelCellStyles.format_filter)
        format_title = book.add_format(ReportExcelCellStyles.format_title)
        format_content_text = book.add_format(ReportExcelCellStyles.format_content_text)
        format_content_number = book.add_format(ReportExcelCellStyles.format_content_number)
        format_content_date = book.add_format(ReportExcelCellStyles.format_content_date)
        format_content_float = book.add_format(ReportExcelCellStyles.format_content_float)

        rowx = 0
        # create report object
        report_excel_output_obj = self.env['oderp.report.excel.output'].with_context(filename_prefix=('kpi_report'), form_title=file_name).create({})

        # create sheet
        sheet = book.add_worksheet(report_name)
        sheet.set_landscape()
        sheet.set_paper(9)  # A4
        sheet.set_margins(0.78, 0.39, 0.39, 0.39)  # 2cm, 1cm, 1cm, 1cm
        sheet.fit_to_pages(1, 0)
        sheet.set_footer('&C&"Times New Roman"&9&P', {'margin': 0.1})

        # compute column
        sheet.set_column('A:A', 4)
        sheet.set_column('B:B', 15)
        sheet.set_column('C:C', 15)
        sheet.set_column('D:D', 15)
        sheet.set_column('E:E', 15)
        sheet.set_column('F:F', 9)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 20)
        sheet.set_column('I:I', 5.5)
        sheet.set_column('J:J', 9)
        sheet.set_column('K:K', 15)
        sheet.set_column('L:L', 20)
        sheet.set_column('M:M', 9)

        rowx += 2
        sheet.merge_range(rowx, 4, rowx, 10, 'Ажилчдын сарын KPI үнэлгээ', format_name)
        rowx += 2
        sheet.merge_range(rowx, 0, rowx, 5, '%s: %s' % ('Байгууллагын нэр:', self.write_uid.company_id.name), format_filter)
        rowx += 1

        sheet.merge_range(rowx, 0, rowx, 5, '%s: %s' % ('KPI нэр: ', self.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 5, '%s: %s' % ('KPI Сар:', self.period_id.name), format_filter)
        rowx += 1
        sheet.merge_range(rowx, 0, rowx, 2, '%s: %s' % (_('Хэвлэсэн огноо'), time.strftime('%Y-%m-%d')), format_content_date)
        rowx += 2

        sheet.merge_range(rowx, 0, rowx + 1, 0, '№', format_title)
        sheet.merge_range(rowx, 1, rowx + 1, 1, 'Ажилтаны овог', format_title)
        sheet.merge_range(rowx, 2, rowx + 1, 2, 'Ажилтаны нэр', format_title)
        sheet.merge_range(rowx, 3, rowx + 1, 3, 'Ажилтаны Регистр', format_title)
        sheet.merge_range(rowx, 4, rowx + 1, 4, 'Албан тушаал', format_title)
        sheet.merge_range(rowx, 5, rowx + 1, 5, 'Жин1', format_title)
        sheet.merge_range(rowx, 6, rowx + 1, 6, 'Үнэлгээ1', format_title)
        sheet.merge_range(rowx, 7, rowx + 1, 7, 'Тэмдэглэл', format_title)
        sheet.merge_range(rowx, 8, rowx + 1, 8, 'Баталсан хэрэглэгч1', format_title)
        sheet.merge_range(rowx, 9, rowx + 1, 9, 'Жин2', format_title)
        sheet.merge_range(rowx, 10, rowx + 1, 10, 'Үнэлгээ2', format_title)
        sheet.merge_range(rowx, 11, rowx + 1, 11, 'Тэмдэглэл2', format_title)
        sheet.merge_range(rowx, 12, rowx + 1, 12, 'Баталсан хэрэглэгч2', format_title)
        sheet.merge_range(rowx, 13, rowx + 1, 13, 'Нийт kpi', format_title)
        rowx += 2

        sequence = 1
        for line in self.kpi_lines:

            sheet.write(rowx, 0, sequence, format_content_number)
            sheet.write(rowx, 1, line.employee_id.last_name, format_content_text)
            sheet.write(rowx, 2, line.employee_id.name, format_content_text)
            sheet.write(rowx, 3, line.employee_id.ssnid, format_content_text)
            sheet.write(rowx, 4, line.employee_job_name, format_content_text)
            sheet.write(rowx, 5, line.kpi_percent_weight1, format_content_float)
            sheet.write(rowx, 6, line.kpi_first_value, format_content_float)
            sheet.write(rowx, 7, line.kpi_note1 if line.kpi_note1 else '', format_content_text)
            sheet.write(rowx, 8, line.approve_user_id1.name if line.approve_user_id1 else '', format_content_text)
            sheet.write(rowx, 9, line.kpi_percent_weight2, format_content_float)
            sheet.write(rowx, 10, line.kpi_second_value, format_content_float)
            sheet.write(rowx, 11, line.kpi_note2 if line.kpi_note2 else '', format_content_text)
            sheet.write(rowx, 12, line.approve_user_id2.name if line.approve_user_id2 else '', format_content_text)
            sheet.write(rowx, 13, line.total_kpi, format_content_text)
            rowx += 1
            sequence += 1
    #             name_selection_groups
            # create date
        rowx += 1

        sheet.write(rowx, 3, 'KPI Менежер: ........................................... ', format_filter)
        rowx += 1
        sheet.write(rowx, 3, 'Баталсан дарга: ........................................... ', format_filter)
        rowx += 1
        sheet.write(rowx, 3, 'Хүлээн авсан цалингийн нягтлан: ........................................... ', format_filter)

        book.close()

        report_excel_output_obj.filedata = base64.encodestring(output.getvalue())
        # call export function
        return report_excel_output_obj.export_report()


class AttitudeType(models.Model):
    _name = 'moncon.kpi.line'
    _rec_name = 'employee_id'

    kpi_line_id = fields.Many2one('moncon.kpi', string='KPI id', ondelete='cascade')
    employee_id = fields.Many2one('hr.employee', string='Employee', required=True, index=True)
    employee_name = fields.Char(related="employee_id.name")
    employee_job_name = fields.Char(related="employee_id.job_id.name")
    period_id = fields.Many2one('account.period', string="KPI line month", related='kpi_line_id.period_id')
    kpi_percent_weight1 = fields.Float(string='KPI Weight 1', digits=(3, 2), default=0)
    kpi_first_value = fields.Float(string='KPI Value 1', digits=(3, 2), default=0)
    kpi_note1 = fields.Char(string='Note', size=80)
    approve_user_id1 = fields.Many2one('res.users', string='User1 to approve',)
    kpi_line_state1 = fields.Selection([('draft', 'New'), ('done', 'Approved')], string='Төлөв',)
    kpi_percent_weight2 = fields.Float(string='KPI Weight 2', digits=(3, 2), default=0)
    kpi_second_value = fields.Float(string='KPI Value 2', digits=(3, 2), default=0)
    kpi_note2 = fields.Char(string='Note', size=80)
    approve_user_id2 = fields.Many2one('res.users', string='User2 to approve',)
    total_kpi = fields.Float(string='Total KPI', compute='_compute_total_kpi')
    kpi_line_state2 = fields.Selection([('draft', 'New'), ('done', 'Approved')], string='Төлөв', default='draft')
    show_approve_button1 = fields.Boolean(string='Show Approve Button1?', compute='_show_approve_button1')
    show_approve_button2 = fields.Boolean(string='Show Approve Button2?', compute='_show_approve_button2')
    show_kpi_manager = fields.Boolean(string='Show KPI Manager?', compute='_show_kpi_manager')
    kpi_line_state_form = fields.Selection([('draft', 'New'), ('done', 'Approved'),], readonly=True, related='kpi_line_id.kpi_state', string='State related')
    hr_employee_id = fields.Many2one(related='employee_id.hr_employee_id', store=True, string=u'ХН ажилтан')


    def to_approve_employee_kpi1(self):
        for line in self:
            line.kpi_line_state1 = 'done'

    def to_cancel_employee_kpi1(self):
        for line in self:
            line.kpi_line_state1 = 'draft'

    def to_approve_employee_kpi2(self):
        for line in self:
            line.kpi_line_state2 = 'done'

    def to_cancel_employee_kpi2(self):
        for line in self:
            line.kpi_line_state2 = 'draft'

    #KPI Үнэлгээ 100- аас их, 0-ээс бага байвал хадгалах үед анхааруулга өгөх.
    @api.constrains('kpi_first_value', 'kpi_second_value')
    def _check_kpi_value_percent(self):
        for line in self:
            if line.kpi_first_value > 100 or line.kpi_first_value < 0:
                raise ValidationError(_('Error ! Percent must be between 0 to 100!!!.'))
            elif line.kpi_second_value > 100 or line.kpi_second_value < 0:
                raise ValidationError(_('Error ! Percent must be between 0 to 100!!!.'))
            else:
                return False

    #Нийт KPI Жингүүдээр тооцоолох функц
    @api.depends('kpi_first_value', 'kpi_second_value', 'kpi_percent_weight1', 'kpi_percent_weight2')
    @api.multi
    def _compute_total_kpi(self):
        for line in self:
            if 0 < line.kpi_first_value <= 100 or 0 < line.kpi_second_value <= 100:
                line.total_kpi = ((line.kpi_percent_weight1 * line.kpi_first_value)/100) + ((line.kpi_percent_weight2 * line.kpi_second_value) / 100)

    #Батлах ажилтанд өөрийн талбаруудыг харуулах функц
    @api.multi
    def _show_approve_button1(self):
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        for line in self:
            if line.approve_user_id1 == current_emp.user_id:
                line.show_approve_button1 = True
            else:
                line.show_approve_button1 = False
    #Батлах ажилтанд өөрийн талбаруудыг харуулах функц2
    @api.multi
    def _show_approve_button2(self):
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        for line in self:
            if line.approve_user_id2 == current_emp.user_id:
                line.show_approve_button2 = True
            else:
                line.show_approve_button2 = False

    #KPI менежер бүгдийг харах, засах эрхтэй хэрэглэгч
    @api.multi
    def _show_kpi_manager(self):
        current_emp = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        group_kpi_manager = self.env.ref('l10n_mn_moncon_kpi.group_kpi_hr_manager').id
        user_ids = self.env['res.users'].search([('groups_id', 'in', [group_kpi_manager])])
        for obj in self:
            if current_emp.user_id in user_ids:
                obj.show_kpi_manager = True
            else:
                obj.show_kpi_manager = False






