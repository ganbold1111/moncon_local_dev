# -*- coding: utf-8 -*-
import time
import datetime

from odoo import models, fields, api, _
from odoo.exceptions import UserError


class PaymentCertificateAddTransaction(models.TransientModel):
    _name = "payment.certificate.add.transaction"
    _description = "add payment certificate to payment permit"

    date = fields.Date(string='Төлбөрийн огноо', required=1)
    line_ids = fields.Many2many('payment.certificate', string="Төлбөр зөвшөөрөх хуудас")

    @api.model
    def default_get(self, fields):
        res = super(PaymentCertificateAddTransaction, self).default_get(fields)
        if self._context.get('active_ids'):
            b_obj = self.env['payment.certificate']
            line = b_obj.browse(self._context['active_ids'])
            res.update({
                'line_ids': [(6, 0, line.ids)]
            })
        return res

    def cert_add(self, context=None):
        if context is None:
            context = {}
        certificate_obj = self.env['payment.certificate']
        transaction_obj = self.env['payment.permit']
        active_ids = context.get('active_ids', []) or []
        today = self.date
        company_id = False

        user = self.env['res.users'].browse(self.env.uid)
        for user in self.env['res.users'].browse(self._uid):
            company_id = user.company_id.id
        permissions = self.env['transaction.payment.permission'].search([('date', '=', today), ('company_id', '=', company_id)], limit=1)
        if permissions:
            permission = permissions[0]
        else:
            permission = self.env['transaction.payment.permission'].create({'date': today, 'company_id': company_id})
        # Хэрэв permission_ids байвал
        for cert in certificate_obj.browse(active_ids):
            # if cert.state == 'open':
            # descrip_val = str(cert.number) or ''
            # amount_total = abs(cert.amount_total)
            # residual = abs(cert.residual)
            # paid_total = cert.amount_total - cert.residual
            existings = transaction_obj.search([('cert_id', '=', cert.id), ('state', '=', 'draft'), ('transaction_id.date', '=', today)])

            if existings:
                # Хэрэв payment permit дээр бичилт үүссэн бол
                existings.write({'cert_id': cert.id,
                                 'company_id': cert.company_id.id,
                                 'descrip': str(cert.name) or '',
                                 'date': today,
                                 'res_partner': cert.partner_id.id,
                                 # 'account_id': cert.account_id.id,
                                 'total_amount': cert.other_amount_without_tax,
                                 'currency': cert.currency_id.id,
                                 'date_end': cert.pay_date,
                                 'remaining_value': cert.residual,
                                 'paid_value': 0,
                                 'pay_value': cert.currency_amount,
                                 'contract_id': cert.contract_id.id or False,
                                 'analytic_account_id': cert.expense_category.id or False,
                                 'analytic_account_project_id': cert.expense_category.id or False,
                                 'invoice_date': cert.pay_date,
                                 'comment': cert.note,
                                 })
            else:
                transaction = self.env['payment.permit'].create({'name': cert.name,
                                                                 'cert_id': cert.id,
                                                                 'company_id': cert.company_id.id,
                                                                 'descrip': str(cert.name) or '',
                                                                 'date': today,
                                                                 'res_partner': cert.partner_id.id,
                                                                 'state': 'draft',
                                                                 'currency': cert.currency_id.id,
                                                                 'date_end': cert.pay_date,
                                                                 'remaining_value': cert.residual,
                                                                 'paid_value': 0,
                                                                 'pay_value': cert.currency_amount,
                                                                 'total_amount': cert.other_amount_without_tax,
                                                                 'transaction_id': permission.id,
                                                                 'contract_id': cert.contract_id.id or False,
                                                                 'analytic_account_id': cert.expense_category.id or False,
                                                                 'analytic_account_project_id': cert.expense_category.id or False,
                                                                 'invoice_date': cert.pay_date,
                                                                 'comment': cert.note,
                                                                 'allow_payment_types': cert.allow_payment_types,
                                                                 })
            cert.state = 'fee'
            cert.onchange_other_amount_without_tax()