# -*- coding: utf-8 -*-
{
    'name': "Moncon - Purchase",
    'depends': [
        'mcsp_purchase_requisition',
    ],
    'author': "Asterisk Technologies LLC",
    'website': 'http://asterisk-tech.mn',
    'category': 'Mongolian HR Modules',
    'description': """
        * Худалдан авалтын захиалгын хүсэлтийн мөр дэх
            1) Барааны тоо хэмжээ
            2) Ажлын код
            3) Зардлын код
            4) Бараа-г 
        тухайн худалдан авалтын захиалгын сүүлийн шатаас өмнө/хүсэлтийн мөр дууссан/цуцлагдсанаас бусад төлөвт хариуцсан худалдан авалтын ажилтан болон харгалзах ажлын урсгалын хэрэглэгч засдаг болгов. Зассан хүмүүсийн лог хөтлөдөг болгов.
    """,
    'data': [
        'views/purchase_requisition_product_view.xml',
        'views/purchase_requisition_service_view.xml',
        'views/purchase_requisition_views.xml',
    ],
    'license': 'GPL-3',
    'application': True,
    'installable': True,
    'auto_install': False
}
