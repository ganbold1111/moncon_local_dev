# -*- coding: utf-8 -*-

import base64
from datetime import date, datetime
from io import BytesIO
from lxml import etree  
import pytz
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from odoo.modules import get_module_resource
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

    
class PurchaseRequisitionLine(models.Model):
    _inherit = 'purchase.requisition.line'

    is_final = fields.Boolean(related='requisition_id.status_id.is_final', readonly=True, string='Is Final State')
    is_accessable = fields.Boolean(compute='compute_is_accessable', default="compute_is_accessable")
    is_purchase_emp = fields.Boolean(compute='compute_is_purchase_emp', default="compute_is_purchase_emp")
    
    @api.multi
    def write(self, vals):
        res = super(PurchaseRequisitionLine, self).write(vals)
        
        if 'product_qty' in vals.keys():
            self.message_post(body="%s:\t'%s' ~ %s" % ('Тоо хэмжээг шинэчиллээ:', self.product_id.name, self.product_qty))
            self.requisition_id.message_post(body="%s:\t'%s' ~ %s" % ('Тоо хэмжээг шинэчиллээ:', self.product_id.name, self.product_qty))
        if 'wbs_code_id' in vals.keys():
            self.message_post(body="%s:\t'%s' ~ %s" % ('Ажлын кодыг шинэчиллээ:', self.product_id.name, '[%s] %s' % (self.wbs_code_id.name, self.wbs_code_id.description)))
            self.requisition_id.message_post(body="%s:\t'%s' ~ %s" % ('Ажлын кодыг шинэчиллээ:', self.product_id.name, '[%s] %s' % (self.wbs_code_id.name, self.wbs_code_id.description)))
        if 'cbs_code_id' in vals.keys():
            self.message_post(body="%s:\t'%s' ~ %s" % ('Зардлын кодыг шинэчиллээ:', self.product_id.name, '[%s] %s' % (self.cbs_code_id.name, self.cbs_code_id.description_cbs)))
            self.requisition_id.message_post(body="%s:\t'%s' ~ %s" % ('Зардлын кодыг шинэчиллээ:', self.product_id.name, '[%s] %s' % (self.cbs_code_id.name, self.cbs_code_id.description_cbs)))
        if 'product_id' in vals.keys():
            self.message_post(body="%s:\t'%s' ~ %s" % ('Барааг шинэчиллээ:', self.work_name, self.product_id.name))
            self.requisition_id.message_post(body="%s:\t'%s' ~ %s" % ('Барааг шинэчиллээ:', self.work_name, self.product_id.name))
            
        return res
    
    @api.model
    def create(self, vals):
        res = super(PurchaseRequisitionLine, self).create(vals)
        if res.requisition_id.status_id and not res.requisition_id.status_id.is_start:
            raise ValidationError("Зөвхөн ноорог төлөвтэй захиалгын хүсэлтэд бараа нэмэх боломжтой !!!")
        return res
    
    def compute_is_accessable(self):
        # Тухайн хэрэглэгч ажлын урсгалын сүүлийн шатанд батлах хэрэглэгч мөн эсэхийг шалгах
        for obj in self:
            if obj.requisition_id and obj.requisition_id.workflow_id and obj.requisition_id.workflow_history_ids and not obj.requisition_id.status_id.is_final:
                last_history = self.env['workflow.history'].search([('requisition_id', '=', obj.requisition_id.id)], order='sent_date desc', limit=1)
                obj.is_accessable = True if last_history and self.env.user in last_history.user_ids else False
            elif not obj.requisition_id or (obj.requisition_id and not obj.requisition_id.status_id) or (obj.requisition_id and obj.requisition_id.status_id and obj.requisition_id.status_id.is_start):
                obj.is_accessable = True
            else:
                obj.is_accessable = False
                
            if not obj.is_accessable and obj.requisition_id.purchase_employee_id.user_id == self.env.user and not obj.requisition_id.status_id.is_final:
                obj.is_accessable = True
                
    def compute_is_purchase_emp(self):
        # Тухайн хэрэглэгч ажлын урсгалын сүүлийн шатанд батлах хэрэглэгч мөн эсэхийг шалгах
        for obj in self:
            obj.is_purchase_emp = True if obj.purchase_employee_id.user_id == self.env.user else False
    
