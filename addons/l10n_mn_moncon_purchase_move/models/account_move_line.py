# -*- coding: utf-8 -*-

import logging

from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    purchase_id = fields.Many2one('purchase.order', string='Purchase')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        for obj in self.filtered(lambda x: x.move_id):
            for line in obj.move_id.line_ids:
                if obj.purchase_id:
                    line.update({'purchase_id': obj.purchase_id.id})
                else:
                    if obj.invoice_line_ids:
                        line.update({'purchase_id': obj.invoice_line_ids[0].purchase_line_id.order_id.id})
        return res


class StockMove(models.Model):
    _inherit = 'stock.move'

    def _prepare_account_move_line(self, qty, cost, credit_account_id, debit_account_id):
        res = super(StockMove, self)._prepare_account_move_line(qty, cost, credit_account_id, debit_account_id)
        for i in res:
            i[2].update({'purchase_id': self.purchase_line_id.order_id.id})
        return res

    # @OVERRIDE !!!
    # l10n_mn_stock_account_create_move дээрх функцийг дарж бичив.
    def _create_account_move_line(self, credit_account_id, debit_account_id, journal_id):
        _logger.info(_('Function start: move _create_account_move_line'))
        # Журналын мөр үүсгэх
        values = []
        # Шинжилгээний данс 2, Шинжилгээний тархалт дээр _prepare_account_move_line функцийг ашигласан
        # Шууд insert query бичихэд ШТ үед хэрхэн ашиглах шийдэл байхгүй тул үүнийг ашигласан
        move_lines = []
        if self.accounts_dict:
            for acc_line in self.accounts_dict:
                move_lines.extend(self._prepare_account_move_line(self.product_qty, acc_line.amount, acc_line.account_id.id, debit_account_id))
            #БМ дансны дб бичилтийн мөрүүдийг нэгтгэх
            db_line = None
            for line in move_lines:
                if line[2]['account_id'] == debit_account_id:
                    if not db_line:
                        db_line = line
                    else:
                        db_line[2]['debit'] += line[2]['debit']
                    move_lines.remove(line)
            if db_line:
                move_lines.append((0, 0, db_line[2]))
        else:
            move_lines = self._prepare_account_move_line(self.product_qty, self.price_unit, credit_account_id, debit_account_id)
        if move_lines:
            date = self._context.get('force_period_date', self.date or fields.Date.context_today(self))
            company_id = self.company_id and self.company_id.id or self.env.user.company_id.id
            uid = self.create_uid and self.create_uid.id or self.env.user.id
            account_move_id = self.get_account_move(journal_id, date)
            account_move = self.env['account.move'].browse(account_move_id)
            all_creates = True
            for line in move_lines:
                if line[0] != 0 or line[1] != 0:
                    all_creates = False
                name = line[2]['name'].replace("'", "''")
                ref = name
                if line[2]['ref']:
                    ref = line[2]['ref'].replace("'", "''")
                #    шаардахаар авлага үүсгэж байгаа үед журналын мөр нь харилцагч авахгүй байгаа тул ашиглав.
                account_id = self.env['account.account'].search([('id', '=', line[2]['account_id'])])
                if account_move.partner_id and line[2]['debit'] and account_id.reconcile:
                    partner = account_move.partner_id.id
                elif account_move.partner_id and line[2]['credit'] and account_id.reconcile:
                    partner = account_move.partner_id.id
                else:
                    partner = 'NULL'
                if all_creates:
                    values.append("""(%s, '%s', %s, '%s',
                                      %s, %s, %s, %s, %s, 
                                      %s, %s, %s, 
                                      %s, %s,
                                      '%s', '%s', '%s', '%s', 
                                      %s, %s, %s, 
                                      %s, %s, %s, %s)""" % (
                                      uid, date, uid, date,
                                      company_id, account_move_id, journal_id, line[2]['account_id'], line[2]['analytic_account_id'] if line[2]['analytic_account_id'] else 'NULL',
                                      partner, self.id, line[2]['product_id'] if line[2]['product_id'] else 'NULL',
                                      line[2]['product_uom_id'] if line[2]['product_uom_id'] else 'NULL', line[2]['quantity'],
                                      name, ref, date, date,
                                      line[2]['inventory_id'] if line[2]['inventory_id'] else 'NULL', line[2]['debit'], line[2]['credit'],
                                      line[2]['currency_rate'], line[2]['currency_id'] if line[2]['currency_id'] else 'NULL', line[2]['amount_currency'],
                                      line[2]['purchase_id']
                    ))
            if values:
                # Журнал бичилтийн мөр үүсгэх
                qry = """ INSERT INTO account_move_line (
                                create_uid, create_date, write_uid, write_date,
                                company_id, move_id, journal_id, account_id, analytic_account_id,
                                partner_id, stock_move_id, product_id, 
                                product_uom_id, quantity,
                                name, ref, date, date_maturity, 
                                inventory_id, debit, credit, 
                                currency_rate, currency_id, amount_currency, purchase_id
                            ) VALUES  """
                qry += ", ".join(value for value in values)
                qry += " RETURNING id"
                self._cr.execute(qry)
                # Шинжилгээний бичилт үүсгэх
                results = self.env.cr.fetchall()
                aml_ids = [str(result[0]) for result in results]
                if aml_ids:
                    qry = """ INSERT INTO account_analytic_line (create_uid, create_date, write_uid, write_date, account_id, company_id, 
                                          amount, unit_amount, partner_id, name, ref, 
                                          general_account_id, move_id, product_id, product_uom_id, date, currency_id, amount_currency)
                                        SELECT %s, '%s', %s, '%s', analytic_account_id AS account_id, aml.company_id AS company_id,
                                            aml.credit - aml.debit AS amount, aml.quantity AS unit_amount,
                                            aml.partner_id AS partner_id, aml.name AS name, aml.ref AS ref, 
                                            aml.account_id AS general_account_id, aml.id AS move_id, aml.product_id AS product_id,
                                            aml.product_uom_id AS product_uom_id, aml.date AS date,
                                            aml.currency_id AS currency_id, aml.amount_currency AS amount_currency
                                        FROM account_move_line aml
                                        LEFT JOIN account_analytic_line aal ON aml.id = aal.move_id
                                        WHERE aml.analytic_account_id IS NOT NULL AND aal.id IS NULL AND aml.id IN (%s)
                                        ORDER BY aml.id
                                """ % (uid, date, uid, date, ",".join(aml_ids))
                    self._cr.execute(qry)

                    # Үүссэн бичилтүүдийн one2many талбаруудын утгыг цэнэглэнэ.
                    self._update_account_move_line(aml_ids, move_lines)

        _logger.info(_('Function done: move _create_account_move_line'))
        return True