# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

{
    "name" : "l10n_mn - MCSP Payment Certificate",
    "version" : "1.0",
    "author" : "Asterisk Technologies LLC",
    "description": """Payment Certificate""",
    "website" : True,
    "category" : "Account",
    "depends" : [
        'l10n_mn_moncon_contract',
        'l10n_mn_account',
        'l10n_mn_workflow_config',
        'l10n_mn_report',
        'l10n_mn_hr',
        'l10n_mn_project',
    ],
    "init": [],
    "data" : [
        'wizard/payment_cert_reject_note.xml',
        'security/mcsp_tzh_security.xml',
        'security/ir.model.access.csv',
        'security/payment_rule.xml',
        'views/payment_certificate_view.xml',
        'views/certificate_sequence.xml',
        'views/payment_settings_view.xml',
        'views/payout_list_view.xml',
        'views/payment_certificate_report_view.xml',
        'report/report_view.xml',
        'report/payment_assignment_report_view.xml',
        'report/other_payment_certificate_report_view.xml',
        'views/power_settings_view.xml',
        'views/res_partner_view.xml',
        'views/payout_send.xml',
        'views/split_payment_view.xml',
        'views/bank_statement_view.xml',
        'views/contract_performance_view.xml',
        'email_templates/payment_send_finincial_analyst_email_template.xml',
        'email_templates/payment_cancel_email_template.xml',
        'email_templates/payment_return_email_template.xml',
        'email_templates/payout_list_confirm_email_template.xml',
        'email_templates/payment_confirm_email_template.xml'
    ],
    "demo_xml": [
    ],
    "active": False,
    "installable": True,
}
