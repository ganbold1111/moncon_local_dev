# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _
from lxml import etree
from odoo.exceptions import UserError, ValidationError
from odoo.http import request
from datetime import datetime
import time

class PaymentCeftReport(models.Model):
	_name = 'payment.cert.report'
	name = fields.Char('Name', size=10)

class CostDistribute(models.Model):
	_name = 'cost.distribute.line'

	cost_id = fields.Many2one('payment.certificate','Cost id')
	cost_category = fields.Many2one('account.analytic.account', string='Expense category', required=True, domain="[('type','=','normal')]")
	transaction_value = fields.Char(string='Note',size=124)
	contractor_amount = fields.Float(string='Give amount', required=True)
	
class PaymentCertificateDocument(models.Model):
	_name = 'payment.certificate.document'

	document_id = fields.Many2one('payment.document','Document name')
	file_name = fields.Char('File Name')
	doc_file = fields.Binary('File')

class PaymentLocation(models.Model):
	_name = 'payment.location'

	name = fields.Char('Location Name', required=True)

class StateHistory(models.Model):
	_name = 'state.history.line'

	state_history_id = fields.Many2one('payment.certificate','Step number id')
	step_number = fields.Integer('Step number')
	step_name = fields.Char('Step name', required=True, size=64, readonly=True)
	controllers = fields.Many2many('res.users', 'state_his_line_users_rel', 'u_id', 'his_id', string='Controllers')
	sent_date = fields.Datetime('Sent date', required=True, readonly=True)
	beating = fields.Many2one('res.users','Beating')
	reviewed_date = fields.Datetime('Reviewed date', required=True, readonly=True)
	transferred_state = fields.Selection([('1','Draft'),
										  ('2','Posted by financial analyst'),
										  ('3','Posted by service head'),
										  ('4','Posted by Director'),
										  ('5','Checking the primary document'),
										  ('6','Confirmed'),
										  ('7','Refused'),
										  ('8','Canceled')])


class PaymentDeductionsLine(models.Model):
	_name = 'payment.deductions.line'

	@api.multi
	def _total_perf_qty(self):
		for obj in self:
			obj.deductions_total = obj.deductions_before + obj.deductions_report

	payment_cert_id1 = fields.Many2one('payment.certificate','Deductions Without Tax')
	payment_cert_id2 = fields.Many2one('payment.certificate','Deductions')
	deductions_ids = fields.Many2one('contract.deductions','Definition')
	deductions_total = fields.Float('Total', compute=_total_perf_qty)
	deductions_before = fields.Float('Before')
	deductions_report = fields.Float('To report')

class ContractInformation(models.Model):
	_name = 'contract.information.line'

	contract_information_id = fields.Many2one('payment.certificate','Contract information id')
	contract_information = fields.Char('Contract information')
	calculated_percentage = fields.Float('Calculated percentage')
	main_contract_amount = fields.Float('Main contract amount')
	contract_supplementary_change = fields.Float('Contract supplementary change')
	contract_amount_total = fields.Float('Contract all amount')

class PaymentInformation(models.Model):
	_name = 'payment.information.line'

	@api.multi
	def _total_amount(self):
		for obj in self:
			obj.total_amount = obj.before_amount + obj.current_amount

	payment_information_id = fields.Many2one('payment.certificate','Payment information id')
	payment_information = fields.Char('Payment information')
	before_amount = fields.Float('Before Amount')
	current_amount = fields.Float('Current Amount')
	total_amount = fields.Float('Total Amount', compute=_total_amount)

class ContractPowerSetting(models.Model):
	_name = 'contract.power.settings'
	_rec_name = 'amount_limit'

	amount_limit = fields.Float('Service head limit')
	project_leader_limit = fields.Float('Project leader limit')
	
class payment_certificate(models.Model):
	_name = 'payment.certificate'
	_description = 'Payment Certificate'
	_inherit = ['mail.thread', 'ir.needaction_mixin']
	

	def action_to_confirm(self):
		return self.write({'state':'confirmed'})
	
	@api.multi
	def _total_amount(self):
		for obj in self:
			obj.total_amount = obj.additional_amount + obj.amount_without_tax
			
	@api.multi
	def _compute_additional_amount(self):
		for obj in self:
			additional_amount = 0
			if obj.contract_id and obj.contract_id.is_additional:
				contracts = self.env['contract.management'].search([('main_contract_id','=',obj.contract_id.main_contract_id.id)])
				for contract in contracts:
					if contract.partner_id.is_company:
						additional_amount += contract.amount_without_tax
					else:	
						additional_amount += contract.amount_with_tax
			obj.additional_amount = additional_amount

	@api.multi
	def _compute_after_handover_amount(self):
		for obj in self:
			obj.after_handover_amount = (obj.amount_without_tax*obj.after_handover_rate)/100

	@api.multi
	def _compute_warranty_amount(self):
		for obj in self:
			obj.warranty_amount = (obj.amount_without_tax*obj.warranty_rate)/100

	@api.multi
	def _perf_total_amount(self):
		for obj in self:
			obj.perf_total_amount = obj.before_perf_subtotal + obj.current_perf_subtotal
			
	@api.multi
	def _payment_amount(self):
		for obj in self:
			ded_amount = 0
			current_perf_amount = 0
			
			for line in obj.deductions_line_ids:
				ded_amount += line.deductions_report
			
			if obj.contract_tax_id and obj.contract_tax_id.price_include:
				obj.payment_amount = obj.before_tax_amount
			else:
				obj.payment_amount = obj.before_tax_amount + (obj.custom_tax_amount if obj.is_custom_payment else obj.tax_amount) - ded_amount
			
	@api.multi
	def _before_tax_amount(self):
		for obj in self:
			no_tax_ded_amount = 0
			ded_amount = 0
			current_perf_amount = 0
			
			for line in obj.deductions_no_tax_line_ids:
				no_tax_ded_amount += line.deductions_report
			for line in obj.payment_information_line_ids:
				current_perf_amount += line.current_amount
			obj.before_tax_amount = current_perf_amount - no_tax_ded_amount
	
	@api.multi
	def _compute_total_t_cart_amount(self):
		for obj in self:
			obj.total_t_cart_amount = obj.contract_t_cart_amount + obj.current_t_cart_amount        

	@api.multi
	def _compute_total_barter_amount(self):
		for obj in self:
			obj.total_barter_amount = obj.contract_barter_amount + obj.current_barter_amount        
	
	@api.multi
	def _compute_total_fine_amount(self):
		for obj in self:
			obj.total_fine_amount = obj.contract_fine_amount + obj.current_fine_amount
	
	@api.multi
	def _compute_total_deduction_amount(self):
		for obj in self:
			total_deduction = 0
			for line in obj.deductions_line_ids:
				total_deduction += line.deductions_report
			obj.total_deduction_amount = total_deduction
	
	@api.multi
	def _compute_tax_amount(self):
		for obj in self:
			if obj.allow_payment_types == '1' and obj.contract_performance_id:
				lines = None
				tax_amount = 0
				if not obj.is_custom_payment:
					if obj.contract_performance_id.work_types == 'work':
						lines = obj.contract_performance_id.work_performance_ids
					else:
						lines = obj.contract_performance_id.goods_performance_ids
					for line in lines:
						tax_amount += line.contract_tax_amount
					
					if obj.contract_performance_id.contract_id:
						obj.tax_amount = tax_amount * (1 - obj.contract_performance_id.contract_id.after_handover_rate/100 - obj.contract_performance_id.contract_id.pledge_rate/100)
					else:
						obj.tax_amount = tax_amount
				else:
					obj.tax_amount = obj.custom_tax_amount
			elif obj.allow_payment_types == '1':
				computed_tax_amount = 0
				if obj.contract_tax_id:
					tax_amount = obj.contract_tax_id.compute_all(obj.before_tax_amount)
					if obj.contract_tax_id.price_include:
						computed_tax_amount = tax_amount['total_excluded']
					else:
						computed_tax_amount = tax_amount['total_included']
				
					obj.tax_amount = obj.before_tax_amount - computed_tax_amount if obj.before_tax_amount > computed_tax_amount else computed_tax_amount - obj.before_tax_amount
					
	@api.onchange('after_handover_rate')
	def onchange_handover_rate(self):
		for obj in self:
			obj.after_handover_amount = obj.amount_without_tax * obj.after_handover_rate / 100

	@api.onchange('warranty_rate')
	def onchange_warranty_rate(self):
		for obj in self:
			obj.warranty_amount = obj.amount_without_tax * obj.warranty_rate / 100

	@api.multi
	def _default_datetime_now(self):
		now = datetime.now()
		return now
	
	@api.one
	@api.depends('payment_amount','other_amount_without_tax','allow_payment_types','currency_id', 'payment_information_line_ids','deductions_no_tax_line_ids',
				'before_tax_amount','tax_amount','custom_tax_amount')
	def _get_pay_amount(self):
		self.pay_amount = self.payment_amount if self.allow_payment_types == '1' else self.other_amount_without_tax

	@api.one
	@api.depends('payment_amount','currency_amount','allow_payment_types','currency_id', 'payment_information_line_ids','deductions_no_tax_line_ids',
				'before_tax_amount','tax_amount','custom_tax_amount')
	def _get_pay_amount_mnt(self):
		for obj in self:
			obj.pay_amount_mnt = obj.payment_amount if obj.allow_payment_types == '1' else obj.currency_amount

	@api.depends('amount_pay', 'currency_amount')
	def compute_amount(self):
		for obj in self:
			obj.residual = obj.other_amount_without_tax - obj.amount_pay

	name = fields.Char('Name')
	allow_payment_types = fields.Selection([('1','Бараа материал'),('2','Гэрээт ажлын гүйцэтгэл'),('3', 'Төслийн жижиг зардал'),('4', 'ҮА удирдлагын зардал')], string='Allow payment type', required=True, default='1')
	payment_type = fields.Selection([('1','Inventory order page'),
							  ('2','Cutsoms VAT'),
							  ('3','Next calculations appointments'),
							  ('4','Project other'),
							  ('5','Operating costs'),
							  ('6','Not operating costs'),
							  ('7','Other project'),
							  ('8','Notification of payment of the contract')], string='Payment type', required=True)
	contract_id = fields.Many2one('contract.management', 'Contract', readonly=True)
	project_id = fields.Many2one('project.project', 'Project name',domain = "[('user_id','!=', False)]")
	project_code = fields.Char(related="project_id.concise_name", readonly=True, default=False, store=True)
	pay_date = fields.Date('Payable Date')
	department_id = fields.Many2one('hr.department','Department name')
	partner_id = fields.Many2one('res.partner','Partner', required=True)
	respondent = fields.Many2one('res.users','Respondent', default=lambda self: self.env.user, required=True)
	phone = fields.Char('Phone', default=lambda self: self.env.user.phone)
	choose_director = fields.Many2one('res.users','Choose confirm director')
	financial_id = fields.Many2one('res.users','Financial name')
	director_id = fields.Many2one('res.users', 'Confirm director name')
	origin = fields.Char('Origin')
	decision_number = fields.Char('Decision Number')
	appropriate = fields.Char('Appropriate')
	location_id = fields.Many2one('payment.location','Location')
	company_id = fields.Many2one('res.company', 'Company', required=True, index=True, default=lambda self: self.env.user.company_id.id, readonly=True)
	# expense_lines = fields.One2many('payment.expense.line','certificate_id','Expense Lines')
	
	payment_term_id = fields.Many2one('product.product','Payment Term')
	note = fields.Char('Note', size=128)
	currency_id = fields.Many2one('res.currency','Currency', default=lambda self: self.env.user.company_id.currency_id)
	currency_rate = fields.Float('Currency rate')
	currency_amount = fields.Float('Give amount in MNT', store=True)
	before_tax_amount = fields.Float('Before tax amount', compute=_before_tax_amount)
	# contractor_give_amount = fields.Float('Payment Amount', compute=_compute_contractor_give_amount)
	expense_category = fields.Many2one('account.analytic.account','Expense Category', domain="[('type','=','normal')]")
	cashflow_type = fields.Many2one('account.cashflow.type','Cashflow type', domain="[('type','=','normal')]")
	is_expense_distributing = fields.Boolean('Is expense distributing')

	is_over_budget = fields.Selection([('in_budget','In Budget'),('over_budget','Over Budget'),('no_budget','No budget')],string='Is Over Budget')

	over_budget_amount = fields.Float('Over budget amount')

	payment_out_company_account = fields.Many2one('account.journal','Company account')
	customer_account = fields.Many2one('res.partner.bank', 'Customer account', domain="[('partner_id','=',partner_id)]")
	priority =  fields.Selection([('0','Urgently'), ('1','Normal')], 'Priority', default='1')

	cost_distribute_ids = fields.One2many('cost.distribute.line','cost_id','Cost distribute line')
	state_history_ids = fields.One2many('state.history.line','state_history_id','State history ids')
#============================Гэрээний ТЗХ талбарууд================================
	state_commission_release_collateral_act = fields.Binary('State commission release collateral act')
	quality_assurance_release_loan_act = fields.Binary('Quality assurance release loan act')
	amount_without_tax = fields.Float('Main contract amount', readonly=True)
	additional_amount = fields.Float('Additional amount', readonly=True, compute=_compute_additional_amount)
	total_amount = fields.Float('Contract Total amount', readonly=True, compute=_total_amount)
	after_handover_rate = fields.Float('Rate collateral after hand over')
	after_handover_amount = fields.Float('Amount collateral after hand over', compute=_compute_after_handover_amount)
	warranty_rate = fields.Float('Warranty rate')
	warranty_amount = fields.Float('Warranty amount', compute=_compute_warranty_amount)
	before_perf_subtotal = fields.Float('Before performance subtotal')
	current_perf_subtotal = fields.Float('Current performance subtotal')
	perf_total_amount = fields.Float('Performance total amount', readonly=True, compute=_perf_total_amount)
	payment_amount = fields.Float('Payment amount', readonly=True, compute=_payment_amount)

	total_deduction_amount = fields.Float('Total deduction amount', readonly=True, compute=_compute_total_deduction_amount)
	
	contract_tax_id = fields.Many2one('account.tax','Tax')
	tax_amount = fields.Float('Tax amount', compute=_compute_tax_amount)
	custom_tax_amount = fields.Float('Custom tax amount')
	
	is_custom_payment = fields.Boolean('Is Custom Payment')
	
	contract_t_cart_amount = fields.Float('Contract T cart amount', readonly=True)
	current_t_cart_amount = fields.Float('Current T cart amount')
	total_t_cart_amount = fields.Float('Total T cart amount', readonly=True, compute=_compute_total_t_cart_amount)
	
	contract_barter_amount = fields.Float('Contract barter amount', readonly=True)
	current_barter_amount = fields.Float('Current barter amount')
	total_barter_amount = fields.Float('Total barter amount', readonly=True, compute=_compute_total_barter_amount)
	
	contract_fine_amount = fields.Float('Contract fine amount', readonly=True)
	current_fine_amount = fields.Float('Current fine amount')
	total_fine_amount = fields.Float('Total fine amount', readonly=True, compute=_compute_total_fine_amount)
	
	partner_bank_id = fields.Many2one('res.partner.bank','Partner account', domain="[('partner_id','=',partner_id)]")
	# partner_bank_acc_id = fields.Many2one('res.partner.bank','Partner Bank Account')
	company_bank_id = fields.Many2one('account.journal','Company Bank')
	
	tax_id = fields.Selection([('1','VAT 10%'),('2','Other tax')],'Tax')
	
	currency_rate_date = fields.Datetime('Currency rate date', default=_default_datetime_now)
	amount_with_tax = fields.Float('Give amount')

	other_amount_without_tax = fields.Float('Total amount')

	contract_performance_id = fields.Many2one('contract.performance', 'Contract performance')

	payout_id = fields.Many2one('payout.list', 'Payout list id', copy=False)
	
	deductions_no_tax_line_ids = fields.One2many('payment.deductions.line','payment_cert_id1','Deductions Without Tax')
	deductions_line_ids = fields.One2many('payment.deductions.line','payment_cert_id2','Deductions')
	payment_information_line_ids = fields.One2many('payment.information.line','payment_information_id','Payment information line')
	amount_pay = fields.Float(string='Төлсөн дүн')
	residual = fields.Float(compute='compute_amount', store=True, string='Үлдэгдэл дүн')
	state = fields.Selection([('draft', 'Draft'),
							  ('project_accountant', 'Төслийн нябо'),
							  ('project_economist', 'Төслийн эдийн засагч'),
							  ('accountant', 'Ерөнхий нябо'),
							  ('finance_director', 'Санхүүгийн захирал'),
							  ('director', 'ҮА харицсан захирал'),
							  ('cancel', 'Canceled'),
							  ('finished', 'Finished'),
							  ('fee', 'Гарах төлбөр'),
							  ('complete', 'Төлөгдсөн')], readonly=True, default='draft', copy=False, string="State",
							 track_visibility='onchange')

	pay_amount_mnt = fields.Float('Pay amount MNT', compute='_get_pay_amount_mnt', store=True)
	pay_amount = fields.Float('Pay amount', compute='_get_pay_amount', store=True)

	@api.onchange('other_amount_without_tax', 'amount_pay', 'residual')
	@api.depends('other_amount_without_tax', 'amount_pay', 'residual')
	def onchange_other_amount_without_tax(self):
		self.currency_amount = self.other_amount_without_tax - self.amount_pay

	@api.multi
	def write(self, vals):
		if 'contract_id' in vals:
			contract_obj = self.env['contract.management'].browse(vals['contract_id'])
			
			vals.update({
				# 'project_id': contract_obj.project_id.id,
				'department_id': contract_obj.department_id.id,
				'partner_id': contract_obj.partner_id.id,
				'amount_without_tax': contract_obj.main_contract_id.amount_without_tax if contract_obj.is_additional else contract_obj.amount_without_tax,
				'company_bank_id': contract_obj.project_id.journal_id.id if contract_obj.project_id else contract_obj.department_id.journal_id.id,
			})
			if contract_obj.procurement_employee:
				if contract_obj.procurement_employee.user_id:
					self.add_follower('payment.certificate', self.id, contract_obj.procurement_employee.user_id.partner_id)
				else:
					raise UserError((u'Энэ гэрээний худалдан авалтын ажилтны Холбоотой хэрэглэгч талбар нь хоосон байна.'))
			
		if 'partner_id' in vals:
			partner_obj = self.env['res.partner'].browse(vals['partner_id'])
			if partner_obj.bank_ids:
				for acc in partner_obj.bank_ids:
					self.partner_bank_id = acc.id

		if 'project_id' in vals:
			project_obj = self.env['project.project'].browse(vals['project_id'])
			self.company_bank_id = project_obj.journal_id.id
			
		if 'respondent' in vals:
			user = self.env['res.users'].browse(vals['respondent'])
			self.add_follower('payment.certificate', self.id, user.partner_id)
			
		return super(payment_certificate, self).write(vals)

	@api.multi
	def set_name(self, prefix, company_id):
		name = u''
		count_pr = self.search_count([('company_id', '=', company_id)])
		sequence = "%04.f"%int(count_pr+1)

		# PC20190625/9992
		name = prefix + '/' + str(datetime.now().strftime('%Y%m%d')) + '/' + sequence
		return name

	@api.model
	def create(self, vals):
		vals['name'] = self.env['ir.sequence'].get('payment.certificate')

		if vals['contract_id']:
			contract_obj = self.env['contract.management'].browse(vals['contract_id'])
			
			vals.update({
				# 'project_id': contract_obj.project_id.id,
				'department_id': contract_obj.department_id.id,
				'partner_id': contract_obj.partner_id.id,
				'amount_without_tax': contract_obj.main_contract_id.amount_without_tax if contract_obj.is_additional else contract_obj.amount_without_tax,
				'company_bank_id': contract_obj.project_id.journal_id.id if contract_obj.project_id else contract_obj.department_id.journal_id.id,
			})
		this = super(payment_certificate, self).create(vals)

		if not vals['name']:
			this.write({'name': self.set_name('PC', this.company_id.id)})

		if not this.contract_id:
			if this.project_id:
				this.company_bank_id = this.project_id.journal_id.id
			elif this.department_id:
				this.company_bank_id = this.department_id.journal_id.id
		else:
			if this.contract_id.procurement_employee:
				if this.contract_id.procurement_employee.user_id:
					self.add_follower('payment.certificate', this.id, this.contract_id.procurement_employee.user_id.partner_id)
				else:
					raise UserError((u'Энэ гэрээний худалдан авалтын ажилтны Холбоотой хэрэглэгч талбар нь хоосон байна.'))

		if this.partner_id:
			if this.partner_id.bank_ids:
				for acc in this.partner_id.bank_ids:
					this.partner_bank_id = acc.id
		
		if 'respondent' in vals:
			user = self.env['res.users'].browse(vals['respondent'])
			
			follower = self.env['mail.followers'].search([('res_model','=','payment.certificate'),('res_id','=',this.id),('partner_id','=',user.partner_id.id)])
			if not follower:
				follower = self.env['mail.followers'].create({
					'subtype_ids':[(6, 0, [1])],
					'res_model': 'payment.certificate',
					'res_id': this.id,
					'partner_id': user.partner_id.id
				})
				
		return this
	
	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super(payment_certificate, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		
		my_group_exdir = self.env.ref('l10n_mn_moncon_contract.group_execute_director').id
		my_group_dep_der = self.env.ref('l10n_mn_moncon_contract.group_first_deputy_director').id
		doc = etree.XML(res['arch'])
		user_ids = self.env['res.users'].search([('groups_id','in',[my_group_exdir,my_group_dep_der])])
		users = []
		for user in user_ids:
			users.append(user.id)

		for node in doc.xpath("//group/group/field[@name='choose_director']"):
			user_filter =  "[('id', 'in'," + str(users) + " )]"
			node.set('domain',user_filter)       
			res['arch'] = etree.tostring(doc)
		return res

	@api.onchange('allow_payment_types')
	def onchange_allow_payment_types(self):
		if self.allow_payment_types == '1':
			self.payment_type = '1'
		elif self.allow_payment_types == '2':
			self.payment_type = '8'
		elif self.allow_payment_types == '3':
			self.payment_type = '4'
		elif self.allow_payment_types == '4':
			self.payment_type = '5'

	@api.onchange('contract_id')
	def onchange_contract_id(self):
		self.project_id = self.contract_id.project_id.id
		self.department_id = self.contract_id.department_id.id
		self.partner_id = self.contract_id.partner_id.id
		self.amount_without_tax = self.contract_id.main_contract_id.amount_without_tax if self.contract_id.is_additional else self.contract_id.amount_without_tax
		self.after_handover_rate = self.contract_id.after_handover_rate
		self.after_handover_amount = self.contract_id.after_handover_amount
		self.warranty_rate = self.contract_id.pledge_rate
		self.warranty_amount = self.contract_id.pledge_amount
		self.expense_category = self.contract_id.analytic_account_id.id
		self.company_bank_id = self.contract_id.project_id.journal_id.id

	@api.onchange('department_id')
	def onchange_department_id(self):
		self.expense_category = self.department_id.analytic_account_id.id
		self.company_bank_id = self.department_id.journal_id.id

	# @api.one
	# def action_to_confirm(self):
	#   return self.write({'state': 'approved'})
		
	@api.one
	def action_to_draft(self):
		return self.write({'state': 'draft'})

####Эдийн засагч ажилтаны хэсэг

	def action_to_send(self):
		for payment in self:
			payment.director_id = payment.write_uid.id
			model_obj = self.env['ir.model.data']
			ctx = {
				'number': payment.name,
				'project': payment.project_id.name if payment.project_id else payment.department_id.name,
				'partner': payment.partner_id.name,
				'allow_payment_types': dict(self._fields['allow_payment_types'].selection).get(payment.allow_payment_types),
				'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
				'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
				'id': payment.id,
				'db_name': request.session.db,
				'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')

			if payment.project_id:
				if payment.allow_payment_types in ('1', '3'):
					if payment.project_id.booker_ids:
						users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids])
						user_emails = []
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
					else:
						raise ValidationError(
							_('Төсөл дээр нягтлан бодогчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))
				elif payment.allow_payment_types == '2':
					if payment.project_id.financial_analyst_ids:
						users = self.env['res.users'].browse([i.id for i in payment.project_id.financial_analyst_ids])
						user_emails = []
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
					else:
						raise ValidationError((u'Төслийн эдийн засагч олдсонгүй. Админтай холбогдоно уу.'))
				elif payment.allow_payment_types == '4':
					my_group_dep_der = self.env.ref('l10n_mn_moncon_contract.group_senior_financial_analyst').id
					user_ids = self.env['res.users'].search([('groups_id', 'in', [my_group_dep_der])])
					user_emails = []
					if user_ids:
						for user in user_ids:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
							email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
							payment.message_post(body=email)
					else:
						raise ValidationError((u'Ерөнхий нягтлан олдсонгүй. Админтай холбогдоно уу.'))
			elif payment.department_id:
				if payment.department_id.booker_ids:
					users = self.env['res.users'].browse([i.id for i in payment.department_id.booker_ids])
					user_emails = []
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
								'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post(body=email)
				else:
					raise ValidationError(
						_('Албан дээр нягтлан бодогчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))
			if payment.allow_payment_types == '1':
				payment.state = 'project_accountant'
			elif payment.allow_payment_types == '2':
				payment.state = 'project_economist'
			elif payment.allow_payment_types == '3':
				payment.state = 'project_accountant'
			elif payment.allow_payment_types == '4':
				payment.state = 'accountant'

	def reverse(self):
		for obj in self:
			if obj.allow_payment_types == '1':
				if obj.state == 'director':
					obj.state = 'project_accountant'
				elif obj.state == 'project_accountant':
					obj.state = 'draft'
			elif obj.allow_payment_types == '2':
				if obj.state == 'finance_director':
					obj.state = 'project_economist'
				elif obj.state == 'project_economist':
					obj.state = 'draft'
			elif obj.allow_payment_types == '3':
				if obj.state == 'accountant':
					obj.state = 'project_accountant'
				elif obj.state == 'project_accountant':
					obj.state = 'draft'
			elif obj.allow_payment_types == '4':
				if obj.state == 'accountant':
					obj.state = 'draft'

	def approve(self):
		for payment in self:
			if payment.allow_payment_types == '1':
				payment.state = 'director'
			elif payment.allow_payment_types == '2':
				payment.state = 'finance_director'
			elif payment.allow_payment_types == '3':
				payment.director_id = payment.write_uid.id
				model_obj = self.env['ir.model.data']
				ctx = {
					'number': payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': dict(self._fields['allow_payment_types'].selection).get(payment.allow_payment_types),
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
				}
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				payment.state = 'accountant'
				my_group_dep_der = self.env.ref('l10n_mn_moncon_contract.group_senior_financial_analyst').id
				user_ids = self.env['res.users'].search([('groups_id', 'in', [my_group_dep_der])])
				user_emails = []
				if user_ids:
					for user in user_ids:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
								'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
				else:
					raise ValidationError((u'Ерөнхий нягтлан олдсонгүй. Админтай холбогдоно уу.'))

	def finish(self):
		for payment in self:
			# model_obj = self.env['ir.model.data']
			# ctx = {
			# 	'number': payment.name,
			# 	'project': payment.project_id.name if payment.project_id else payment.department_id.name,
			# 	'partner': payment.partner_id.name,
			# 	'allow_payment_types': dict(self._fields['allow_payment_types'].selection).get(payment.allow_payment_types),
			# 	'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
			# 	'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
			# 	'id': payment.id,
			# 	'db_name': request.session.db,
			# 	'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
			# }
			# template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
			# if payment.allow_payment_types == '1' and payment.state == 'director':
			# 	group_exe_dir = self.env.ref('l10n_mn_moncon_contract.group_execute_director').id
			# 	user_ids = self.env['res.users'].search([('groups_id', 'in', [group_exe_dir])])
			# 	user_emails = []
			# 	if user_ids:
			# 		for user in user_ids:
			# 			user_emails.append(user.login)
			# 			template_id.with_context(ctx).send_mail(user.id, force_send=True)
			# 			email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
			# 					'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
			# 			payment.message_post(body=email)
			# 	else:
			# 		raise ValidationError((u'Үйл ажиллагаа хариуцсан захирал олдсонгүй. Админтай холбогдоно уу.'))
			# 	payment.state = 'fee'
			# elif payment.allow_payment_types == '2' and payment.state == 'finance_director':
			# 	group_exe_dir = self.env.ref('l10n_mn_moncon_contract.group_financial_director').id
			# 	user_ids = self.env['res.users'].search([('groups_id', 'in', [group_exe_dir])])
			# 	user_emails = []
			# 	if user_ids:
			# 		for user in user_ids:
			# 			user_emails.append(user.login)
			# 			template_id.with_context(ctx).send_mail(user.id, force_send=True)
			# 			email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
			# 					'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
			# 			payment.message_post(body=email)
			# 	else:
			# 		raise ValidationError((u'Санхүүгийн захирал олдсонгүй. Админтай холбогдоно уу.'))
			# 	payment.state = 'fee'
			# elif payment.allow_payment_types == '3' and payment.state == 'accountant':
			# 	group_exe_dir = self.env.ref('l10n_mn_moncon_contract.group_senior_financial_analyst').id
			# 	user_ids = self.env['res.users'].search([('groups_id', 'in', [group_exe_dir])])
			# 	user_emails = []
			# 	if user_ids:
			# 		for user in user_ids:
			# 			user_emails.append(user.login)
			# 			template_id.with_context(ctx).send_mail(user.id, force_send=True)
			# 			email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
			# 					'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
			# 			payment.message_post(body=email)
			# 	else:
			# 		raise ValidationError((u'Ерөнхий нябо олдсонгүй. Админтай холбогдоно уу.'))
			# 	payment.state = 'fee'
			# elif payment.allow_payment_types == '4' and payment.state == 'accountant':
			payment.state = 'fee'

			transaction_obj = self.env['payment.permit']
			today = self.pay_date
			permissions = self.env['transaction.payment.permission'].search([('date', '=', today), ('company_id', '=', self.env.user.company_id.id)], limit=1)
			if permissions:
				permission = permissions[0]
			else:
				permission = self.env['transaction.payment.permission'].create({'date': today, 'company_id': self.env.user.company_id.id})
			existings = transaction_obj.search([('cert_id', '=', payment.id), ('state', '=', 'draft'), ('transaction_id.date', '=', today)])
			if existings:
				# Хэрэв payment permit дээр бичилт үүссэн бол
				existings.write({'cert_id': payment.id,
								 'company_id': payment.company_id.id,
								 'descrip': str(payment.name) or '',
								 'date': today,
								 'res_partner': payment.partner_id.id,
								 'total_amount': payment.other_amount_without_tax,
								 'currency': payment.currency_id.id,
								 'date_end': payment.pay_date,
								 'remaining_value': payment.residual,
								 'paid_value': 0,
								 'pay_value': payment.currency_amount,
								 'contract_id': payment.contract_id.id or False,
								 'analytic_account_id': payment.expense_category.id or False,
								 'analytic_account_project_id': payment.expense_category.id or False,
								 'invoice_date': payment.pay_date,
								 'comment': payment.note,
								 })
			else:
				transaction = self.env['payment.permit'].create({'name': payment.name,
																 'cert_id': payment.id,
																 'company_id': payment.company_id.id,
																 'descrip': str(payment.name) or '',
																 'date': today,
																 'res_partner': payment.partner_id.id,
																 'state': 'draft',
																 'currency': payment.currency_id.id,
																 'date_end': payment.pay_date,
																 'remaining_value': payment.residual,
																 'paid_value': 0,
																 'pay_value': payment.currency_amount,
																 'total_amount': payment.other_amount_without_tax,
																 'transaction_id': permission.id,
																 'contract_id': payment.contract_id.id or False,
																 'analytic_account_id': payment.expense_category.id or False,
																 'analytic_account_project_id': payment.expense_category.id or False,
																 'invoice_date': payment.pay_date,
																 'comment': payment.note,
																 'allow_payment_types': payment.allow_payment_types,
																 })

####Санхүүгийн шинжээчийн хэсэг
	#@api.multi
	def action_to_service_head(self):
		for payment in self:
			if not payment.is_over_budget:
				raise UserError(_('Please select budget state.'))
	
			if payment.is_over_budget != 'in_budget' and not payment.choose_director:
				raise UserError(_('Please select an approved director.'))
	
			payment.financial_id = payment.write_uid.id
			model_obj = self.env['ir.model.data']
			ctx = {    
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			   }
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
	
			if payment.department_id.manager_id.user_id.id:
				users = self.env['res.users'].browse(payment.department_id.manager_id.user_id.id) 
				user_emails = []    
				for user in users:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return payment.write({'state': 'reviewed'})

	# @api.multi
	def action_to_project_leader(self):
		for payment in self:
			if not payment.is_over_budget:
				raise UserError(_('Please select budget state.'))
	
			if payment.is_over_budget != 'in_budget' and not payment.choose_director:
				raise UserError(_('Please select an approved director.'))
	
			payment.financial_id = payment.write_uid.id
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			   }
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
	
			if payment.project_id.user_id.id:
				users = self.env['res.users'].browse(payment.project_id.user_id.id) 
				user_emails = []    
				for user in users:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post(body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return payment.write({'state': 'send_project_leader'})

	def action_to_financial_analyst_return(self, reason):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'reason': reason,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_return_email_template')
			user_emails = []
			users = self.env['res.users'].search([('partner_id','in',[follower.id for follower in payment.message_partner_ids])])
			if users:
				for user in users:
					if user.login not in user_emails:
						user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				if payment.respondent and payment.respondent.id not in users.ids:
					user_emails.append(payment.respondent.login)
					template_id.with_context(ctx).send_mail(payment.respondent.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return payment.write({'state':'draft'})
		
	def action_to_accountant(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
				'number': payment.name,
				'project': payment.project_id.name if payment.project_id else payment.department_id.name,
				'partner': payment.partner_id.name,
				'allow_payment_types': payment.allow_payment_types,
				'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
				'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
				'db_name': request.session.db,
				'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
			}

			#####Төсөвтэй нягтланд илгээнэ
			if payment.is_over_budget == 'in_budget':

				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.project_id.booker_ids:
					users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids])
					user_emails = []
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
								'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post(body=email)
				else:
					raise ValidationError((u'Энэ төсөлд нягтлан бодогч сонгогдоогүй байна. Админтай холбогдоно уу.'))

				payment.write({'state': 'finished'})
			#####Төсөвгүй захиралд илгээнэ
			if payment.is_over_budget == 'no_budget':

				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.choose_director.id:
					users = self.env['res.users'].browse(payment.choose_director.id)
					user_emails = []
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
								'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post(body=email)
				else:
					raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))

				payment.write({'state': 'send_director'})

			#####Төсөв хэтэрсэн эрх мэдэлд нь хүрч байвал шууд нягтланд илгээнэ. Хүрэхгүй байвал захиралд илгээгдэнэ
			if payment.is_over_budget == 'over_budget':
				for power in self.env['contract.power.settings'].search([]):
					if payment.over_budget_amount > power.project_leader_limit:

						template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
						if payment.choose_director.id:
							users = self.env['res.users'].browse(payment.choose_director.id)
							user_emails = []
							for user in users:
								user_emails.append(user.login)
								template_id.with_context(ctx).send_mail(user.id, force_send=True)
							email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
										'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
							payment.message_post(body=email)
						else:
							raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))

						payment.write({'state': 'send_director'})

					else:
						template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')

						if payment.project_id.booker_ids:
							users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids])
							user_emails = []
							for user in users:
								user_emails.append(user.login)
								template_id.with_context(ctx).send_mail(user.id, force_send=True)
							email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
										'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
							payment.message_post(body=email)
						else:
							raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))

						payment.write({'state': 'finished'})

####Албаны даргын хэсэг
	def action_to_service_head_confirm(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {    
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender':  self.env['res.users'].search([('id','=', self._uid)]).name,
			   }
	#####Төсөвтэй нягтланд илгээнэ
			if payment.is_over_budget == 'in_budget':
				
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.project_id:
					if payment.project_id.booker_ids:
						users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids]) 
						user_emails = []    
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True) 
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post( body=email)
					else:
						raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
				elif payment.department_id:
					if payment.department_id.booker_ids:
						users = self.env['res.users'].browse([i.id for i in payment.department_id.booker_ids]) 
						user_emails = []    
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True) 
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post( body=email)
					else:
						raise ValidationError((u'Хэлтэс дээр нягтлан бодогчийг сонгоогүй. Админтай холбогдоно уу.'))
	
				payment.write({'state':'recieving_primary_document'})
	#####Төсөвгүй захиралд илгээнэ
			if payment.is_over_budget == 'no_budget':
	
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.choose_director.id:
					users = self.env['res.users'].browse( payment.choose_director.id)
					user_emails = []    
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True) 
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post( body=email)
				else:
					raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
				payment.write({'state': 'send_director'})
	
	#####Төсөв хэтэрсэн эрх мэдэлд нь хүрч байвал шууд нягтланд илгээнэ. Хүрэхгүй байвал захиралд илгээгдэнэ
			if payment.is_over_budget == 'over_budget':
				for power in self.env['contract.power.settings'].search([]):
					if payment.over_budget_amount > power.project_leader_limit:
						
						template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
						
						if payment.choose_director.id:
							users = self.env['res.users'].browse( payment.choose_director.id)
							user_emails = []
							for user in users:
								user_emails.append(user.login)
								template_id.with_context(ctx).send_mail(user.id, force_send=True) 
							email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
							payment.message_post( body=email)
						else:
							raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
						payment.write({'state': 'send_director'})
	
					else:
						template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
						if payment.project_id:
							if payment.project_id.booker_ids:
								users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids]) 
								user_emails = []    
								for user in users:
									user_emails.append(user.login)
									template_id.with_context(ctx).send_mail(user.id, force_send=True)  
								email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
								payment.message_post( body=email)
							else:
								raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
						elif payment.department_id:
							if payment.department_id.booker_ids:
								users = self.env['res.users'].browse([i.id for i in payment.department_id.booker_ids]) 
								user_emails = []    
								for user in users:
									user_emails.append(user.login)
									template_id.with_context(ctx).send_mail(user.id, force_send=True) 
								email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
								payment.message_post( body=email)
							else:
								raise ValidationError((u'Хэлтэс дээр нягтлан бодогчийг сонгоогүй. Админтай холбогдоно уу.'))
	
						payment.write({'state': 'recieving_primary_document'})
					
			if payment.allow_payment_types == '1':
				if payment.contract_id and payment.contract_id.create_user_id:
					users = self.env['res.users'].browse(payment.contract_id.create_user_id.id) 
					user_emails = []
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)  
					email = u'' + u'.\n Гэрээний ажилтанд имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post( body=email)
				else:
					raise ValidationError((u'Гэрээний ажилтан олдсонгүй. Админтай холбогдоно уу.'))
			return True
				
#####Албаны дарга цуцлах
	def action_to_service_head_cancel(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_cancel_email_template')
	
			if payment.respondent.id:
				users = self.env['res.users'].browse(payment.respondent.id) 
				user_emails = []    
				for user in users:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True) 
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return payment.write({'state':'cancel'})

#####Албаны дарга буцаах
	def action_to_service_head_return(self, reason):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url':  self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'reason': reason,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_return_email_template')
			user_emails = []
	
			users = self.env['res.users'].search([('partner_id','in',[follower.id for follower in payment.message_partner_ids])])
			if users:
				for user in users:
					if user.login not in user_emails:
						user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				if payment.respondent and payment.respondent not in users:
					user_emails.append(payment.respondent.login)
					template_id.with_context(ctx).send_mail(payment.respondent.id, force_send=True) 
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return payment.write({'state':'draft'})


####Төслийн менежерийн хэсэг
	def action_to_project_leader_confirm(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {    
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			   }
			
	#####Төсөвтэй нягтланд илгээнэ  
			if payment.is_over_budget == 'in_budget':
	
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.project_id.booker_ids:
					users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids]) 
					user_emails = []    
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True) 
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post( body=email)
				else:
					raise ValidationError((u'Энэ төсөлд нягтлан бодогч сонгогдоогүй байна. Админтай холбогдоно уу.'))
	
				payment.write({'state':'finished'})
	#####Төсөвгүй захиралд илгээнэ
			if payment.is_over_budget == 'no_budget':
	
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.choose_director.id:
					users = self.env['res.users'].browse(payment.choose_director.id)
					user_emails = []
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)
					email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post( body=email)
				else:
					raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
				payment.write({'state': 'send_director'})
	
	#####Төсөв хэтэрсэн эрх мэдэлд нь хүрч байвал шууд нягтланд илгээнэ. Хүрэхгүй байвал захиралд илгээгдэнэ
			if payment.is_over_budget == 'over_budget':
				for power in self.env['contract.power.settings'].search([]):
					if payment.over_budget_amount > power.project_leader_limit:
						
						template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
						if payment.choose_director.id:
							users = self.env['res.users'].browse(payment.choose_director.id)
							user_emails = []
							for user in users:
								user_emails.append(user.login)
								template_id.with_context(ctx).send_mail(user.id, force_send=True)  
							email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
							payment.message_post( body=email)
						else:
							raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
						payment.write({'state': 'send_director'})
	
					else:
						template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
	
						if payment.project_id.booker_ids:
							users = self.env['res.users'].browse([i.id for i in payment.project_id.booker_ids]) 
							user_emails = []    
							for user in users:
								user_emails.append(user.login)
								template_id.with_context(ctx).send_mail(user.id, force_send=True)  
							email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
							payment.message_post( body=email)
						else:
							raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
						payment.write({'state': 'send_director'})
			
			#Гэрээний ажилтанд мэдэгдэл илгээх
			
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_confirm_email_template')
			if payment.allow_payment_types == '1':
				if payment.contract_id and payment.contract_id.create_user_id:
					users = self.env['res.users'].browse(payment.contract_id.create_user_id.id) 
					user_emails = []    
					for user in users:
						user_emails.append(user.login)
						template_id.with_context(ctx).send_mail(user.id, force_send=True)  
					email = u'' + u'.\n Гэрээний ажилтанд имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
					payment.message_post( body=email)
				else:
					raise ValidationError((u'Гэрээний ажилтан олдсонгүй. Админтай холбогдоно уу.'))
			
			return True

#####Төслийн мененжер цуцлах
	def action_to_project_leader_cancel(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url':  self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_cancel_email_template')
	
			if payment.respondent.id:
				users = self.env['res.users'].browse(payment.respondent.id) 
				user_emails = []    
				for user in users:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)  
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return self.write({'state':'cancel'})
	
	#####Төслийн мененжер буцаах
	def action_to_project_leader_return(self, reason):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'reason': reason,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_return_email_template')
			user_emails = []
	
			users = self.env['res.users'].search([('partner_id','in',[follower.id for follower in payment.message_partner_ids])])
				
			if users:
				for user in users:
					if user.login not in user_emails:
						user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True) 
				if payment.respondent and payment.respondent not in users:
					user_emails.append(payment.respondent.login)
					template_id.with_context(ctx).send_mail(payment.respondent.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return self.write({'state':'draft'})

####Захиралын хэсэг
	def action_to_director_confirm(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
				'number': payment.name,
				'project': payment.project_id.name if payment.project_id else payment.department_id.name,
				'partner': payment.partner_id.name,
				'allow_payment_types': payment.allow_payment_types,
				'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
				'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
				'id': payment.id,
				'db_name': request.session.db,
				'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_confirm_email_template')

			if payment.create_uid.id:
				users = self.env['res.users'].browse(payment.create_uid.id)
				user_emails = []
				for user in users:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
							'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post(body=email)
			else:
				raise ValidationError(_('Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))

			return self.write({'state': 'finished'})

#####Захирал цуцлах
	def action_to_director_cancel(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_cancel_email_template')
	
			if payment.respondent.id:
				users = self.env['res.users'].browse(payment.respondent.id) 
				user_emails = []    
				for user in users:
					user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)  
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError(_('Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return self.write({'state':'cancel'})

#####Захирал буцаах
	def action_to_director_return(self, reason):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'reason': reason,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_return_email_template')
			
			user_emails = []
			users = self.env['res.users'].search([('partner_id','in',[follower.id for follower in payment.message_partner_ids])])
				
			if users:
				for user in users:
					if user.login not in user_emails:
						user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)  
				if payment.respondent and payment.respondent not in users:
					user_emails.append(payment.respondent.login)
					template_id.with_context(ctx).send_mail(payment.respondent.id, force_send=True)
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError(_('Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return self.write({'state':'draft'})

######Нягтлангийн хэсэг
	def action_to_primary_document_receipt(self):
		for payment in self:
			model_obj = self.env['ir.model.data']
			account_group = model_obj.get_object_reference('account', 'group_account_user')

			res_user = self.env['res.users'].search([('groups_id', 'in', account_group[1])])

			ctx = {
				'number': payment.name,
				'project': payment.project_id.name if payment.project_id else payment.department_id.name,
				'partner': payment.partner_id.name,
				'allow_payment_types': payment.allow_payment_types,
				'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
				'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
				'id': payment.id,
				'db_name': request.session.db,
				'sender': self.env['res.users'].search([('id', '=', self._uid)]).name,
			}

			# if payment.allow_payment_types == '2':
			# 	if payment.payment_type == '8':
			# 		raise UserError(_(
			# 			'The type of transaction on other payment permits may not be subject to the Payment Charge Statement.'))

			if payment.allow_payment_types == '2':
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.project_id:
					if payment.project_id.financial_analyst_ids:
						users = self.env['res.users'].browse([i.id for i in payment.project_id.financial_analyst_ids])
						user_emails = []
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
					else:
						raise ValidationError((u'Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))

				elif payment.department_id:
					if payment.department_id.financial_analyst_ids:
						users = self.env['res.users'].browse(
							[i.id for i in payment.department_id.financial_analyst_ids])
						user_emails = []
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
					else:
						raise ValidationError(
							(u'Энэ хэлтэст санхүүгийн шинжээчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))

				return payment.write({'state': 'posted_financial_ananlyst'})

			if payment.allow_payment_types == '1':
				template_id = self.env.ref('l10n_mn_moncon_tzh.payment_send_finincial_analyst_email_template')
				if payment.project_id:
					if payment.project_id.financial_analyst_ids:
						users = self.env['res.users'].browse([i.id for i in payment.project_id.financial_analyst_ids])
						user_emails = []
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
					else:
						raise ValidationError(
							(u'Энэ төсөлд санхүүгийн шинжээчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))
				elif payment.department_id:
					if payment.department_id.financial_analyst_ids:
						users = self.env['res.users'].browse(
							[i.id for i in payment.department_id.financial_analyst_ids])
						user_emails = []
						for user in users:
							user_emails.append(user.login)
							template_id.with_context(ctx).send_mail(user.id, force_send=True)
						email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + (
									'<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
						payment.message_post(body=email)
					else:
						raise ValidationError(
							(u'Энэ хэлтэст санхүүгийн шинжээчийг сонгож өгөөгүй байна. Админтай холбогдоно уу.'))

				return payment.write({'state': 'posted_financial_ananlyst'})

	def action_to_primary_document_receipt_return(self, reason):
		for payment in self:
			model_obj = self.env['ir.model.data']
			ctx = {
					'number':payment.name,
					'project': payment.project_id.name if payment.project_id else payment.department_id.name,
					'partner': payment.partner_id.name,
					'allow_payment_types': payment.allow_payment_types,
					'base_url': self.env['ir.config_parameter'].sudo().get_param('web.base.url'),
					'action_id': model_obj.get_object_reference('l10n_mn_moncon_tzh', 'action_payment_certificate')[1],
					'id': payment.id,
					'db_name': request.session.db,
					'reason': reason,
					'sender': self.env['res.users'].search([('id','=', self._uid)]).name,
			}
			template_id = self.env.ref('l10n_mn_moncon_tzh.payment_return_email_template')
			
			user_emails = []
			users = self.env['res.users'].search([('partner_id','in',[follower.id for follower in payment.message_partner_ids])])
			if users:
				for user in users:
					if user.login not in user_emails:
						user_emails.append(user.login)
					template_id.with_context(ctx).send_mail(user.id, force_send=True)
				if payment.respondent and payment.respondent not in users:
					user_emails.append(payment.respondent.login)
					template_id.with_context(ctx).send_mail(payment.respondent.id, force_send=True)   
				email = u'' + u'.\n Дараах хэрэглэгчид рүү имэйл илгээгдэв: ' + ('<b>' + ('</b>, <b>'.join(user_emails)) + '</b>')
				payment.message_post( body=email)
			else:
				raise ValidationError(_('Хүлээн авах хүн олдсонгүй. Админтай холбогдоно уу.'))
	
			return self.write({'state': 'draft'})

	@api.onchange('project_id')
	def onchange_depertment(self):
		if self.project_id.journal_id:
			self.company_bank_id = self.project_id.journal_id.id

	def add_follower(self, res_model, res_id, follower):
		user = self.env.user
		mail_obj = self.env['mail.followers']
		is_follower = mail_obj.search([('partner_id', '=', follower.id),('res_model','=',res_model),('res_id','=',res_id)])

		if not is_follower:
			return self.env['mail.followers'].create({
				'subtype_ids':[(6, 0, [1])],
				'res_model': res_model,
				'res_id': res_id,
				'partner_id': follower.id
			})
		return True
	
	@api.multi
	def action_to_create_payout_list(self):
		curr_id = self.currency_id.id
		rate_id = self.env['res.currency.rate'].search([('currency_id', '=', curr_id), ('name', '<=', time.strftime("%Y-%m-%d"))], order="name desc", limit=1)
		rate = None
		if rate_id:
			rate = rate_id.alter_rate or 0.0
		else:
			rate = 1.0
		docs = []

		if self.attach_docs:
			docs = [doc.id for doc in self.attach_docs]
		
		cost_distribute = []
		if self.cost_distribute_ids:
			cost_distribute = [dist.id for dist in self.cost_distribute_ids]

		val = \
			{
				'payment_id': self.id,
				'contract_id': self.contract_id.id if self.contract_id else None,
				'contract_name': self.contract_id.name if self.contract_id else '',
				'project_id':self.project_id.id if self.project_id else None,
				'department_id':self.department_id.id if self.department_id else None,
				'partner_id': self.partner_id.id,
				'pay_date': self.pay_date,
				'transaction_value': self.note,
				'amount_with_tax': self.payment_amount if self.allow_payment_types == '1' else self.other_amount_without_tax,
				'mnt_amount': self.currency_amount,
				'currency_id': self.currency_id.id,
				'company_currency_id': self.env.user.company_id.currency_id.id,
				'expense_category': self.expense_category.id,
				'payout_company_account': self.company_bank_id.id,
				'partner_account': self.partner_bank_id.id,
				'cashflow_type': self.cashflow_type.id if self.cashflow_type else None,
				'is_expense_distributing': self.is_expense_distributing,
				#'payment_docs': [(6,0,[docs])] if docs else None,
				'attachment_docs': [(6,0,[docs])] if docs else None,
				'cost_distribution': [(6,0,[cost_distribute])] if cost_distribute else None,
				}
		payout_id = self.env['payout.list'].create(val)
# 		
# 		for follower in self.message_follower_ids:
# 			payout_id.message_subscribe_users(user_ids=follower.id)
		
		self.payout_id = payout_id
		form_res = self.env['ir.model.data'].get_object_reference('l10n_mn_moncon_tzh', 'l10n_mn_payout_list_form_view')
		form_id = form_res and form_res[1] or False
		tree_res = self.env['ir.model.data'].get_object_reference('l10n_mn_moncon_tzh', 'l10n_mn_payout_list_form_view')
		tree_id = tree_res and tree_res[1] or False
		return {
			'name': _('Payout list'),
			'view_type': 'form',
			'view_mode': 'form,tree',
			'res_model': 'payout.list',
			'res_id': payout_id.id,
			'view_id': False,
			'views': [(form_id, 'form'), (tree_id, 'tree')],
			'type': 'ir.actions.act_window',
		}


	@api.multi
	def show_payout_list(self):
		action = self.env.ref('l10n_mn_moncon_tzh.l10n_mn_payout_list_action').read()[0]
		action['views'] = [(self.env.ref('l10n_mn_moncon_tzh.l10n_mn_payout_list_form_view').id, 'form')]
		action['res_id'] = self.payout_id.id
		return action

class PaymentCertificateDocument(models.Model):
	_inherit = 'payment.certificate.document'

	payment_id = fields.Many2one('payment.certificate','Payment Certificate')
	
class IrAttachment(models.Model):
	_inherit = 'ir.attachment'

	document_id = fields.Many2one('payment.document','Document name')
	payment_id = fields.Many2one('payment.certificate','Payment Certificate')

class payment_certificates(models.Model):
	_inherit = 'payment.certificate'

	payment_docs = fields.One2many('payment.certificate.document', 'payment_id', string='Payment documents')
	attach_docs = fields.One2many('ir.attachment', 'payment_id', string='Payment documents')

	@api.onchange('payment_type')
	def onchange_payment_type(self):
		#
		# if self.allow_payment_types == '2':
		# 	if self.payment_type == '8':
		# 		raise UserError(_('The type of transaction on other payment permits may not be subject to the Payment Charge Statement.'))
		
		payment_type_obj = self.env['payment.type'].search([('name', '=', self.payment_type)], limit=1)
		
		values = {}
		lines = []
		if payment_type_obj:
			for doc in payment_type_obj.required_document:
				lines.append([0,0,{'document_id': doc.id,  'name': u'%s'%(doc.name)}])
		self.attach_docs = lines
		return {'values': values}
	
	@api.multi
	def unlink(self):
		for payment in self:
			if payment.state not in ('draft', 'cancel'):
				raise ValidationError(_('You cannot delete a payment which is not draft or cancelled.'))
		return super(payment_certificates, self).unlink()	
