# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################
from odoo import models, fields, api, _
import time
import datetime
from odoo.tools.translate import _
from odoo.exceptions import UserError, ValidationError

class payout_list_send(models.Model):
    _name = "payout.list.send"
    _description = "Payout List Send"

    @api.multi
    def action_send(self):
        for payouts in self:
            lines=self.env['payout.list'].browse(self._context['active_ids'])
            valid_line_state = True
            for line in lines:
                if line.state != 'expected':
                    valid_line_state = False
            if not valid_line_state:
                raise UserError(_('All lines must be in "Expected" state'))
            else:
                for line in lines:
                    line.write({'state' : 'sent_to_director', 'action_date_txt': time.strftime("%Y-%m-%d")})

class payout_list_accept(models.Model):
    _name = "payout.list.accept"
    _description = "Payout List Accept"

    @api.multi
    def action_accept(self):
        for payouts in self:
            lines=self.env['payout.list'].browse(self._context['active_ids'])
            valid_line_state = True
            for line in lines:
                if line.state != 'sent_to_director':
                    valid_line_state = False
            if not valid_line_state:
                raise UserError(_('All lines must be in "Sent to Director" state'))
            else:
                for line in lines:
                    line.write({'state' : 'accepted', 'action_date_txt': time.strftime("%Y-%m-%d")})
        
class payout_list_pay(models.Model):
    _name = "payout.list.pay"
    _description = "Payout List Pay"

    @api.multi
    def action_pay(self):
        for payouts in self:
            lines=self.env['payout.list'].browse(self._context['active_ids'])
            valid_line_state = True
            for line in lines:
                if not line.payout_company_account:
                    raise UserError(_('Please select company journal.'))
                
                if line.state != 'accepted':
                    valid_line_state = False
                    
            if not valid_line_state:
                raise UserError(_('All lines must be in "Accepted" state'))
            else:
                line_ids = []
                statement_dic = {}
                for line in lines:
                    line_ids.append(line.id)
                    
                    account_id = self.env['account.account'].search([('user_type_id.type','=','expense')], limit=1)
                    
                    if account_id:
                        acc_id = account_id.id
                    else:
                        raise ValidationError(_('Зардлын данс олдсонгүй.'))
                    
                    if line.payout_company_account.id not in statement_dic.keys():
                        bank_statement_id = self.env['account.bank.statement'].search([('journal_id','=',line.payout_company_account.id),('state','=','open'),('date','=', line.paid_date if line.paid_date else time.strftime("%Y-%m-%d"))])
                        
                        if not bank_statement_id:
                            statement_id = self.env['account.bank.statement'].search([('journal_id','=',line.payout_company_account.id),('state','=','confirm')], limit=1, order='date desc')
                            balance_start = 0
                            if statement_id:
                                statement = statement_id
                                balance_start = statement.balance_end
                                                    
                            vals = {
                                'date': line.paid_date if line.paid_date else time.strftime("%Y-%m-%d"),
                                'journal_id': line.payout_company_account.id,
                                'balance_start': balance_start,
                                'state': 'open',
                                'currency': line.currency_id.id,
                                }
                            bank_statement_id = self.env['account.bank.statement'].create(vals)
    
                        statement_dic[line.payout_company_account.id] = bank_statement_id.id
                        
                    cost_distribute = []
                    if line.cost_distribution:
                        cost_distribute = [dist.id for dist in line.cost_distribution]
                    elif line.expense_category:
                        dist_id = self.env['cost.distribute.line'].create({'cost_category': line.expense_category.id,
                                                                        'transaction_value': line.name,
                                                                        'contractor_amount': line.currency_rate * line.amount_with_tax })
                        cost_distribute = [dist_id.id]
                    vals = {
                        'partner_id': line.partner_id.id,
                        'account_id': acc_id if acc_id else None,
                        'statement_id': statement_dic[line.payout_company_account.id],
                        'journal_id': line.payout_company_account.id,
                        'currency_id': line.currency_id.id,
                        'currency_rate_paid': line.currency_rate,
                        'currency_rate_date_paid': line.currency_rate_date,
                        'amount': line.currency_rate * line.amount_with_tax * (-1),
                        'date': line.paid_date if line.paid_date else time.strftime("%Y-%m-%d"),
                        'name': line.transaction_value,
                        'cashflow_id': line.cashflow_type.id,
                        'cost_distribution': [(6,0,[cost_distribute])] if cost_distribute else None,
                        }
                    
                    self.env['account.bank.statement.line'].create(vals)
                    line.write({
                                'state': 'paid',
                                  'paid_date': time.strftime("%Y-%m-%d") if not line.paid_date else line.paid_date, 
                                  'mnt_amount_paid': line.currency_rate * line.amount_with_tax,
                                  'currency_rate_paid': line.currency_rate,
                                  'currency_rate_date_paid': line.currency_rate_date,
                                  'statement_id': statement_dic[line.payout_company_account.id],
                                  'action_date_txt': time.strftime("%Y-%m-%d")
                                  })
                return True
            
