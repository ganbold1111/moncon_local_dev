# -*- coding: utf-8 -*-
##############################################################################
#
#    Asterisk Technologies LLC, Enterprise Management Solution    
#    Copyright (C) 2013-2013 Asterisk Technologies LLC (<http://www.erp.mn>). All Rights Reserved
#
#    Email : info@asterisk-tech.mn
#    Phone : 976 + 77228080
#
##############################################################################

from odoo import models, fields, api, _

class res_partner (models.Model):
    _inherit = 'res.partner'

    director_id = fields.Many2one('res.partner', 'Director')
    representative_id  = fields.Many2one('res.partner','Representative')
    bank_id = fields.Many2one('res.partner.bank','Bank name, account number', domain="[('partner_id','=',id)]")

class res_partner_bank(models.Model):
    _inherit = 'res.partner.bank'

    def name_get(self):
        res = []
        for record in self:
            name = record.acc_number
            if record.bank_id:
                name = record.bank_id.name +' - '+ record.acc_number
            res.append((record.id, name))
        return res

    journal_id = fields.Many2one('account.journal', string='Journal')
